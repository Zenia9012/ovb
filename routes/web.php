<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::get('login/facebook', 'Auth\SocialAuthController@redirect')->name('login.redirect.facebook');
Route::get('login/facebook/callback', 'Auth\SocialAuthController@callback')->name('login.callback.facebook');
Route::get('login/google', 'Auth\SocialAuthController@redirect')->name('login.redirect.google');
Route::get('login/google/callback', 'Auth\SocialAuthController@callback')->name('login.callback.google');
Route::post('login/social/email', 'Auth\SocialAuthController@addEmail')->name('login.social.email');
Route::post('login/telegram', 'Auth\SocialAuthController@addTelegramId')->name('login.telegram');

// teacher
Route::prefix('teachers')->name('teacher.')->group(function() {

    Route::namespace('Teacher')->middleware(['teacher', 'verified'])->group(function() {

        Route::get('profile/create', 'ProfileController@create')->name('profile.create');
        Route::post('profile', 'ProfileController@store')->name('profile.store');

        Route::middleware(['teacher.profile.exist'])->group(function (){

            Route::get('/cabinet', 'CabinetController@index')->name('cabinet.index');
            // profile
            Route::get('profile', 'ProfileController@index')->name('profile.index');
            Route::get('profile/edit', 'ProfileController@edit')->name('profile.edit');
            Route::put('profile', 'ProfileController@update')->name('profile.update');

            // invite student
            Route::get('invite', 'InviteController@index')->name('invite.index');
            Route::post('invite', 'InviteController@store')->name('invite.store');
            Route::delete('invite/{invite}', 'InviteController@destroy')->name('invite.destroy');

            // student request
            Route::get('students/requests', 'StudentRequestController@index')->name('students.requests.index');
            Route::put('students/requests/{teacherRequests}', 'StudentRequestController@update')->name('students.requests.update');

            // courses
            Route::get('courses/my-courses', 'CourseController@myCourses')->name('courses.my-courses');
            Route::get('courses', 'CourseController@index')->name('courses.index');
            Route::get('courses/create', 'CourseController@create')->name('courses.create');
            Route::get('courses/{course}', 'CourseController@show')->name('courses.show');
            Route::post('courses', 'CourseController@store')->name('courses.store');

            //lessons
            Route::get('courses/{course}/chapters/{chapter}/lessons/create', 'LessonController@create')->name('lessons.create');
            Route::get('courses/{course}/chapters/{chapter}/lessons/{lesson}/edit', 'LessonController@edit')->name('lessons.edit');
            Route::post('courses/{course}/chapters/{chapter}/lessons', 'LessonController@store')->name('lessons.store');
            Route::put('courses/{course}/chapters/{chapter}/lessons/{lesson}', 'LessonController@update')->name('lessons.update');

            // individual lessons themes
            Route::resource('private/themes', 'IndividualLessonThemeController')
                ->only('index', 'store', 'show', 'update', 'destroy')
                ->middleware(['throttle:60,1', 'teacher']);

            // individual lessons
            Route::get('private/lessons', 'IndividualLessonsController@index')->name('individual.index');
            Route::get('private/lessons/create', 'IndividualLessonsController@create')->name('individual.create');
            Route::get('private/lessons/{lesson}/edit', 'IndividualLessonsController@edit')->name('individual.edit');
            Route::put('private/lessons/{lesson}', 'IndividualLessonsController@update')->name('individual.update')->middleware('throttle:30,1');
            Route::post('private/lessons', 'IndividualLessonsController@store')->name('individual.store');
            Route::delete('private/lessons/{lesson}', 'IndividualLessonsController@destroy')->name('individual.destroy');
            Route::post('private/lessons/{lesson}/group', 'IndividualLessonsController@addLessonToGroup')->name('individual.addLessonToGroup');
            Route::post('private/lessons/{lesson}/student', 'IndividualLessonsController@addLessonToStudent')->name('individual.addLessonToStudent');
            Route::put('private/lessons/{lesson}/activate', 'IndividualLessonsController@activate')->name('individual.activate')->middleware('throttle:30,1');
            Route::post('private/lessons/{lesson}/duplicate', 'IndividualLessonsController@duplicate')->name('individual.duplicate')->middleware('throttle:15,1');
            Route::delete('private/lessons/{lesson}/detach/{clientId}', 'IndividualLessonsController@detachStudent')->name('individual.detachStudent')->middleware('throttle:30,1');

            // groups
            Route::get('groups', 'GroupController@index')->name('groups.index');
            Route::post('groups', 'GroupController@store')->name('groups.store');
            Route::put('groups/{group}', 'GroupController@update')->name('groups.update');
            Route::delete('groups/{group}', 'GroupController@destroy')->name('groups.destroy');
            Route::delete('groups/{group}/detach/{clientId}', 'GroupController@detachClient')->name('groups.detachClient');
            Route::post('groups/{group}/attach/{clientId}', 'GroupController@attachClient')->name('groups.attachClient');

            //course lessons
            Route::get('courses/{course}/chapters/{chapter}/lessons/create', 'LessonController@create')->name('lessons.create');
            Route::get('courses/{course}/chapters/{chapter}/lessons/{lesson}/edit', 'LessonController@edit')->name('lessons.edit');
            Route::post('courses/{course}/chapters/{chapter}/lessons', 'LessonController@store')->name('lessons.store');
            Route::put('courses/{course}/chapters/{chapter}/lessons/{lesson}', 'LessonController@update')->name('lessons.update');
        });
    });


    Auth::routes(['verify' => true]);
});

// client
Route::middleware(['client', 'verified'])->namespace('Client')->group(function () {
    Route::get('cabinet', 'CabinetController@index')->name('cabinet.index');

    // profile
    Route::get('profile', 'ProfileController@index')->name('profile.index');
    Route::get('profile/create', 'ProfileController@create')->name('profile.create');
    Route::post('profile', 'ProfileController@store')->name('profile.store');
    Route::get('profile/edit', 'ProfileController@edit')->name('profile.edit');
    Route::put('profile', 'ProfileController@update')->name('profile.update');

    // courses
    Route::get('courses/my-courses', 'CourseController@show')->name('courses.show');
    Route::get('courses', 'CourseController@index')->name('courses.index');

    // teachers
    Route::get('teachers', 'TeacherController@index')->name('teachers.index');
    Route::get('teachers/{teacher}', 'TeacherController@show')->name('teachers.show');
    Route::post('teachers/request', 'TeacherController@requestToTeacher')->name('teachers.requestToTeacher');

    // my-lessons
    Route::get('my-lessons', 'MyLessonController@index')->name('my-lessons.index');
});

Auth::routes(['verify' => true]);

Route::put('courses/{course}/bookmarks', 'Teacher\CourseController@bookmarks')->name('courses.bookmarks')->middleware('throttle:30,1');

