<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('courses/sub-category', 'Teacher\CourseController@getSubCategory')->name('courses.sub-category');
Route::delete('courses/{course}', 'Teacher\CourseController@destroy')->name('courses.destroy');

Route::post('/chapters', 'Teacher\ChapterController@store')->name('chapter.store');
Route::put('/chapters/{chapter}', 'Teacher\ChapterController@update')->name('chapters.update');
Route::put('/chapters/orders', 'Teacher\ChapterController@changeOrder')->name('chapters.changeOrder')->middleware('throttle:30,1');
Route::delete('/chapters/{chapter}', 'Teacher\ChapterController@destroy')->name('chapters.destroy');

Route::put('/lessons/orders', 'Teacher\LessonController@changeOrder')->name('lessons.changeOrder')->middleware('throttle:30,1');
Route::delete('/lessons/{lesson}', 'Teacher\LessonController@destroy')->name('lessons.destroy');
