<?php

namespace Tests\Browser\ClientTest;

use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class AuthTest extends DuskTestCase
{
    /**
     * register client
     *
     * @return void
     * @throws \Throwable
     */
    public function testRegister()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/register')
                ->type('email', 'test' . str_random() . '@gmail.com')
                ->type('name', 'test')
                ->type('password', '12341234')
                ->type('password_confirmation', '12341234')
                ->press('Register')
                ->assertPathIs('/email/verify');
        });
    }

    /**
     * login for client
     *
     * @throws \Throwable
     */
    public function testLogin()
    {
        $this->browse(function (Browser $browser)  {
            $browser->visit('/login')
                ->type('#email', 'client@gmail.com')
                ->type('password', '12341234')
                ->press('Login')
                ->assertPathIs('/cabinet');
        });
    }
}
