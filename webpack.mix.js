const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/template-content.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css').version();

mix.js('resources/js/teacher.js', 'public/js')
    .sass('resources/sass/teacher.scss', 'public/css').version();

mix.js('resources/js/vue.js', 'public/js').version();

mix.js('resources/js/client.js', 'public/js')
    .sass('resources/sass/client.scss', 'public/css').version();

// mix.browserSync('http://127.0.0.1:8000/');
