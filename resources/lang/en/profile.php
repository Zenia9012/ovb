<?php

return [

    'first_name' => 'First name',
    'last_name' => 'Last name',
    'phone' => 'Phone',
    'degree' => 'Degree',
    'chose_skills' => 'Choose your skills',
    'is_public' => 'Public account',
    'personal_info' => 'Personal info',
    'save_btn' => 'Save',
    'req_fields' => 'Required fields:',
    'opt_fields' => 'Optional fields:',
    'note' => 'Note:',
    'field_is_opt' => 'This field is optional',
    'field_is_req' => 'This field is required',
    'at_your_request' => 'At your request, you can fill it out.',
    'describe_y_exper' => 'Describe your experience',
    'public_info' => 'If your account is public, all users of the system will see you',


];
