<?php

return [

    'request_was_sent' => 'Request was sent',
    'message_for_your' => 'Message for you',
    'request_accepted' => 'Request is accepted',
    'request_declined' => 'Request is declined',

];
