<?php

return [

    'link_to_course' => 'Go to course',
    'add_course' => 'Add Course',
    'add.select_category' => 'Select category',
    'add.select_sub_category' => 'Select sub category',
    'add.select_Language' => 'Select language',
    'add.title' => 'Title',
    'add.description' => 'Description',
    'add.content' => 'Content',
    'add.price' => 'Price',
    'add.limit' => 'People Limit',

    'introduction_chapter' => 'Introduction',
    'add_chapter' => 'Add Chapter',
    'add_lesson' => 'Add Lesson',
    'welcome_lesson_title' => 'Welcome',
    'welcome_lesson_desc' => 'Welcome',

    'add_lesson_title' => 'Title',
    'add_lesson_desc' => 'Description',
    'add_lesson_content' => 'Content',
    'add_lesson_duration' => 'Duration',

];
