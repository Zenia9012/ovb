@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Add you email') }}</div>

                <div class="card-body" style="color: black">
                    <div class="alert alert-success" role="alert">
                        {{ __('We don\'t get an email from social login, but email is required.') }}
                    </div>

                    <form class="d-inline" method="POST" action="{{ route('login.social.email') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <h3 style="color: black">{{ $message }}</h3>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('Add') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
