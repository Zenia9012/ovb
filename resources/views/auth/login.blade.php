@extends('layouts.auth')

@section('content')
    <div id="content" style="padding: 0">
        <div class="row">
            <div class="col-50" style="background-color: violet"></div>
            <div class="col-50">
                <div class="register-page">
                    <h1>{{ __('Login') }}</h1>

                    <form method="POST" class="auth-form" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">
                            <input
                                id="email"
                                type="email"
                                class="form-field @error('email') form-field-error @enderror"
                                name="email"
                                required
                                autocomplete="email"
                                placeholder="E-mail"
                            >

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input
                                id="password"
                                type="password"
                                class="form-field @error('password') form-field-error @enderror"
                                name="password"
                                required
                                autocomplete="new-password"
                                placeholder="{{ __('New password') }}"
                            >

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-grey">
                                {{ __('Login') }}
                            </button>
                        </div>

                    </form>

                    @if (Route::has('password.request'))
                        <div>
                            <a class="forgot-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        </div>
                    @endif

                    <div class="form-group text-center">
                        <a href="{{url('login/facebook')}}">
                            <button type="submit" class="btn btn-facebook">
                                <img src="{{ asset('img/icons/facebook_icon.png') }}" alt="facebook icon">
                                {{ __('Login with Facebook') }}
                            </button>
                        </a>

                        <a href="{{url('login/google')}}">
                            <button type="submit" class="btn btn-google">
                                <img src="{{ asset('img/icons/google_icon.png') }}" alt="google icon">
                                {{ __('Login with Google') }}
                            </button>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
