@if(session()->has('NOTIFY_SESSION_KEY'))
    @php
        $i = 0;
    @endphp
    @foreach(session()->pull('NOTIFY_SESSION_KEY') as $type => $messages)
        @php
            $onLoop = 100;
        @endphp
        @if($type == 'NOTIFY_SUCCESS')
            @foreach($messages as $message)
                @php
                    $i++;
                    $top = $i * $onLoop;
                @endphp
                <div class="notify-message notify-message-success" style="top: {{ $top }}px">
                    <div class="icon-notify">
                        <img src="{{ asset('img/notify/success_notify.png') }}" alt="notify info">
                    </div>
                    <div>
                        <div class="notify-title">Успіх!</div>
                        <div>{{ $message }}</div>
                    </div>
                </div>
            @endforeach
        @endif
        @if($type == 'NOTIFY_ERROR')
            @foreach($messages as $message)
                @php
                    $i++;
                    $top = $i * $onLoop;
                @endphp
                <div class="notify-message notify-message-error" style="top: {{ $top }}px">
                    <div class="icon-notify">
                        <img src="{{ asset('img/notify/error_notify.png') }}" alt="notify info">
                    </div>
                    <div>
                        <div class="notify-title">Помилка!</div>
                        <div>{{ $message }}</div>
                    </div>
                </div>
            @endforeach
        @endif
        @if($type == 'NOTIFY_INFO')
            @foreach($messages as $message)
                @php
                    $i++;
                    $top = $i * $onLoop;
                @endphp
                <div class="notify-message notify-message-info" style="top: {{ $top }}px">
                    <div class="icon-notify">
                        <img src="{{ asset('img/notify/info_notify.png') }}" alt="notify info">
                    </div>
                    <div>
                        <div class="notify-title">Інфо!</div>
                        <div>{{ $message }}</div>
                    </div>
                </div>
            @endforeach
        @endif
        @if($type == 'NOTIFY_WARNING')
            @foreach($messages as $message)
                @php
                    $i++;
                    $top = $i * $onLoop;
                @endphp
                <div class="notify-message notify-message-warning" style="top: {{ $top }}px">
                    <div class="icon-notify">
                        <img src="{{ asset('img/notify/warning_notify.png') }}" alt="notify info">
                    </div>
                    <div>
                        <div class="notify-title">Увага!</div>
                        <div>{{ $message }}</div>
                    </div>
                </div>
            @endforeach
        @endif
    @endforeach
@endif

{{-- JS blocks--}}
{{--warning--}}
<div id="js-notify-message-warning" class="notify-message notify-message-warning" style="display: none; top: 60px">
    <div class="icon-notify">
        <img src="{{ asset('img/notify/warning_notify.png') }}" alt="notify info">
    </div>
    <div>
        <div class="notify-title">Увага!</div>
        <div class="notify-message-text-warning"></div>
    </div>
</div>
{{--info--}}
<div id="js-notify-message-info" class="notify-message notify-message-info" style="display: none; top: 60px">
    <div class="icon-notify">
        <img src="{{ asset('img/notify/info_notify.png') }}" alt="notify info">
    </div>
    <div>
        <div class="notify-title">Інфо!</div>
        <div class="notify-message-text-info"></div>
    </div>
</div>
{{--error--}}
<div id="js-notify-message-error" class="notify-message notify-message-error" style="display: none; top: 60px">
    <div class="icon-notify">
        <img src="{{ asset('img/notify/error_notify.png') }}" alt="notify info">
    </div>
    <div>
        <div class="notify-title">Помилка!</div>
        <div class="notify-message-text-error"></div>
    </div>
</div>
{{--success--}}
<div id="js-notify-message-success" class="notify-message notify-message-success" style="display: none; top: 60px">
    <div class="icon-notify">
        <img src="{{ asset('img/notify/success_notify.png') }}" alt="notify info">
    </div>
    <div>
        <div class="notify-title">Успіх!</div>
        <div class="notify-message-text-success"></div>
    </div>
</div>

{{--make sure--}}
<div id="js-notify-make-sure" class="notify-message-warning notify-make-sure" style="display: none;">
    <div class="icon-notify">
        <img src="{{ asset('img/notify/warning_notify.png') }}" alt="notify info">
    </div>
    <div>
        <div class="notify-title">Увага!</div>
        <div class="notify-message-text"></div>

        <div class="notify-buttons">
            <button id="make-sure-okay">Okay</button>
            <button id="make-sure-cancel">Cancel</button>
        </div>

    </div>
</div>
<div class="modal-backdrop" style="display: none"></div>



