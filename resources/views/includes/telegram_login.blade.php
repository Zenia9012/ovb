<script async src="https://telegram.org/js/telegram-widget.js?9" data-telegram-login="sowl_bot" data-size="medium" data-userpic="false" data-radius="5" data-onauth="onTelegramAuth(user)" data-request-access="write"></script>
<script type="text/javascript">

    function onTelegramAuth(user) {
        axios.post('/login/telegram', {
            'telegram_id': user.id,
        }).then(result =>{
            notify('Telegram Added', 'success');
        }).catch(err => {
            notify('Something went wrong, we are working on it', 'error');
        })
    }
</script>
