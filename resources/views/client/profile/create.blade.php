@extends('layouts.client')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create Client profile</div>

                    <div class="card-body">
                        <form action="{{route('profile.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="client-photo">Photo</label>
                                    <input type="file"
                                           class="form-control-file @error('photo') is-invalid @enderror"
                                           id="client-photo"
                                           name="photo"
                                           accept="image/x-png,image/jpg,image/jpeg"
                                           required>

                                    @error('photo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="client-first-name">First name</label>
                                    <input type="text" class="form-control @error('first_name') is-invalid @enderror" id="client-first-name" name="first_name" value="{{ old('first_name') }}" required>

                                    @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="client-last-name">Last name</label>
                                    <input type="text" class="form-control @error('last_name') is-invalid @enderror" id="client-last-name" name="last_name" value="{{ old('last_name') }}" required>

                                    @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-6">
                                    <label for="client-phone">Phone</label>
                                    <input type="text" class="form-control @error('phone') is-invalid @enderror" id="client-phone" name="phone" value="{{ old('phone') }}" required>

                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="col-md-6">
                                    <label for="client-phone">Select your abilities</label>
                                    <select multiple data-placeholder="Select your abilities" name="abilities[]" class="@error('abilities') is-invalid @enderror">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->title}}</option>
                                        @endforeach
                                    </select>

                                    @error('abilities')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
