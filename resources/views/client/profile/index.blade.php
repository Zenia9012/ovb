@extends('layouts.client')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Client profile </div>

                    <div class="card-body">
                        <img src="{{$profile->photo}}" class="rounded mx-auto d-block" style="width: 200px" alt="...">
                       <p class="text-center"><a href="" >Change the photo</a></p>



                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">{{$profile->first_name}}</li>
                            <li class="list-group-item">{{$profile->last_name}}</li>
                            <li class="list-group-item">{{$profile->phone}}</li>
                            <li class="list-group-item">
                                @foreach($profile->client->categories as $category)
                                    <span class="badge badge-secondary">{{$category->title}}</span>
                                @endforeach
                            </li>
                        </ul>

                        <a href="{{route('profile.edit')}}"><button class="btn btn-dark">Change</button></a>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

