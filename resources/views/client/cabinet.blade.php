@extends('layouts.client')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Cabinet</div>

                <ul>
                    <a href="{{ route('profile.index') }}"><li>profile</li></a>
                    <a href="{{ route('courses.show') }}"><li>My courses</li></a>
                    <a href="{{ route('courses.index') }}"><li>All Courses</li></a>
                    <li>bookmarks</li>
                </ul>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
