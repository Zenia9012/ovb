@extends('layouts.client')

@section('bootstrap')

    @include('bootstrap')

@endsection


@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card" style="color: #000">
                    <div class="card-header">Individual access</div>

                    @foreach($data as $item)
                        <div class="card-body">


                            <h2>Teacher: {{ $item->userTeacher->teacherProfile->full_name }}</h2>
                            <hr>
                            <h3>Themes: </h3>
                            <ul>
                                @foreach($item->themes as $theme)
                                    <li><a href="#">{{ $theme->lesson_theme->title }}</a></li>
                                @endforeach
                            </ul>
                            <hr>
                            <h3>Lessons: </h3>
                            <ul>
                                @foreach($item->lessons as $lesson)
                                    <li>
                                        <a href="#">
                                            {{ $lesson->lesson_theme->title }}
                                            @if($completedLessons->contains($lesson->id))
                                                <span class="badge badge-secondary">Done</span>
                                            @endif
                                        </a>
                                    </li>

                                @endforeach
                            </ul>

                        </div>
                    @endforeach
                </div>

                <div class="card" style="color: #000">
                    <div class="card-header">Access by group</div>
                    @foreach($data as $item)
                        <div class="card-body">
                            <h3>Group: </h3>
                            <ul>
                                @foreach($item->groups as $group)
                                    <p>{{ $group->title }}</p>

                                    @foreach($group->themes as $theme)
                                        <li><a href="#">{{ $theme->lesson_theme->title }}</a></li>
                                    @endforeach

                                    @foreach($group->lessons as $lesson)
                                        <li>
                                            <a href="#">
                                                {{ $lesson->lesson_theme->title }}
                                                @if($completedLessons->contains($lesson->id))
                                                    <span class="badge badge-secondary">Done</span>
                                                @endif
                                            </a>
                                        </li>

                                    @endforeach
                                @endforeach
                            </ul>
                            <hr>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

