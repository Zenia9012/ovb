@extends('layouts.client')

@section('bootstrap')

    @include('bootstrap')

@endsection

@section('content')

    <div class="container">

        <div class="row justify-content-center">
            <div class="jumbotron" style="background-color: lightskyblue">
                <h3 class="display-4">{{ $teacher->full_name}}</h3>
                <p class="lead">{{ $teacher->degree }}</p>
                <hr class="my-4">
                <p>{{ $teacher->about_me }}</p>
                <img src="{{asset($teacher->photo)}}" class="rounded bg-black float-left"
                     alt="teacher image">
            </div>
        </div>
    </div>

@endsection

