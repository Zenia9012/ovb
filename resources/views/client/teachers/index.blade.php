@extends('layouts.client')

@section('bootstrap')

    @include('bootstrap')

@endsection

@section('content')

    <teachers-list
        :teachers-data="{{ $teachers }}"
        :requests-data="{{ $requests }}">
    </teachers-list>

@endsection

@section('vue')

    <script src="{{ asset('js/vue.js') }}"></script>

@endsection

