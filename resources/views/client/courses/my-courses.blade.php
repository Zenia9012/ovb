@extends('layouts.client')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                @foreach($courses as $course)
                    <div class="col-sm-6 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">{{$course->info->title}}</h5>
                                <p class="card-text">{{$course->info->description}}</p>
                                <p class="card-text">Lang : {{$course->language->name}}</p>
                                <a href="#" class="btn btn-primary">{{ __('courses.link_to_course') }}</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

