@extends('layouts.teacher')

@section('content')

    <div class="container">
        <div class="row">
            <student-requests :teachers-requests-data="{{ $teachersRequests }}"></student-requests>
        </div>

    </div>

@endsection
@section('vue')

    <script src="{{ asset('js/vue.js') }}"></script>

@endsection

