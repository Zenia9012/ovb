@extends('layouts.teacher')

@section('bootstrap')

    @include('bootstrap')

@endsection
@section('content')

    <div class="container">
        <invite-page :invites-data="{{ $invites }}"></invite-page>
    </div>

@endsection
@section('vue')

    <script src="{{ asset('js/vue.js') }}"></script>

@endsection

