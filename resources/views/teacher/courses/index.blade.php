@extends('layouts.teacher')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <courses-list
                    :categories-data="{{ json_encode($categories) }}"
                    :courses-data="{{ json_encode($courses) }}">

                </courses-list>
            </div>
        </div>
    </div>
@endsection
@section('vue')

    <script src="{{ asset('js/vue.js') }}"></script>

@endsection

