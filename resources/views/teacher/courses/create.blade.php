@extends('layouts.teacher')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <form method="post" action="{{ route('teacher.courses.store') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label for="category">{{ __('courses.add.select_category') }}</label>
                        <select class="form-control" id="category" name="category">
                            <option value="">{{ __('courses.add.select_category') }}</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="sub_category">{{ __('courses.add.select_sub_category') }}</label>
                        <select class="form-control" id="sub_category" name="sub_category[]" multiple></select>

                        @error('sub_category')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="title">{{ __('courses.add.title') }}</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="{{ old('title') }}">

                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="description">{{ __('courses.add.description') }}</label>
                        <textarea class="form-control @error('description') is-invalid @enderror" id="description" rows="3" name="description">{{ old('description') }}</textarea>

                        @error('description')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="input-group mb-3">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="image" name="image" accept="image/*">
                            <label class="custom-file-label" for="image">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text" id="image">Image</span>
                        </div>

                        @error('image')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="content">{{ __('courses.add.content') }}</label>
                        <textarea class="form-control" id="create-course-content" rows="3" name="content">{{ old('content') }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="price">{{ __('courses.add.price') }}</label>
                        <input type="number" step="0.01" class="form-control @error('price') is-invalid @enderror" id="price" name="price" value="{{ old('price') }}">

                        @error('price')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="limit">{{ __('courses.add.limit') }}</label>
                        <input type="number" step="1" class="form-control @error('limit') is-invalid @enderror" id="limit" name="limit" value="{{ old('limit') }}">

                        @error('limit')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="language">{{ __('courses.add.select_Language') }}</label>
                        <select class="form-control @error('language') is-invalid @enderror" id="language" name="language" disabled>
                            @foreach($languages as $language)
                                <option value="{{ $language->id }}">{{ $language->name }}</option>
                            @endforeach
                        </select>

                        @error('language')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('ckeditor')
    <script>
        CKEDITOR.replace( 'create-course-content');
    </script>
@endsection

<div id="reg-txt-top" class="panel-row-style panel-row-style-for-w5d51617351ea5-0">
    <div id="pgc-w5d51617351ea5-0-0" class="panel-grid-cell" data-weight="1">
        <div id="panel-w5d51617351ea5-0-0-0" class="so-panel widget widget_sow-editor panel-first-child panel-last-child" data-index="0" data-style="{&quot;id&quot;:&quot;&quot;,&quot;class&quot;:&quot;&quot;,&quot;widget_css&quot;:&quot;&quot;,&quot;mobile_css&quot;:&quot;&quot;,&quot;margin&quot;:&quot;&quot;,&quot;padding&quot;:&quot;&quot;,&quot;mobile_padding&quot;:&quot;&quot;,&quot;background&quot;:&quot;&quot;,&quot;background_image_attachment&quot;:&quot;0&quot;,&quot;background_image_attachment_fallback&quot;:&quot;&quot;,&quot;background_display&quot;:&quot;tile&quot;,&quot;border_color&quot;:&quot;&quot;,&quot;font_color&quot;:&quot;&quot;,&quot;link_color&quot;:&quot;&quot;}">
            <div class="so-widget-sow-editor so-widget-sow-editor-base">
                <div class="siteorigin-widget-tinymce textwidget">
                    <h1 class="reg_b1_ttl">Product registration</h1>
                    <div class="av_warranty_wrap">
                        <p class="av_warranty">Avologi provides limited warranty for Eneo:</p>
                        <p class="reg_b1_txt">Activate your warranty by registering your product <span class="reg_b1_bttxt">within 10 days of purchase</span>. Viable for products bought on Avologi.com and authorized retailers only.</p>
                        <p class="reg_b1_txt">Disclaimer: No warranty is provided for products sold in third party marketplaces except for the products that the brand owners sell directly. </p>
                        <p class="reg_b1_txt">Registering your AVOLOGI purchase links your product with important notifications, proof of ownership, and warranty service.</p>
                        <p class="reg_b1_bttxt">Registration is quick and easy. Just a few steps and you’re done!</p>

                        <ul class="av_warranty_ul">
                            <li>Lifetime limited warranty for ENEO Advanced, ENEO jewelry edition &amp; ENEO Eye Concentrator</li>
                            <li>2 years limited warranty for ENEO Classic</li>
                        </ul>
                    </div>
                    <p class="reg_b1_subtxt title-register_hidden">Why register your AVOLOGI product?</p>

                </div>
            </div>
        </div>
    </div>
</div>
