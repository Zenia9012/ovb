@extends('layouts.teacher')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-11">
                <div class="media">
                    <img src="{{ asset($course->image) }}" width="200" class="mr-3 img-thumbnail" alt="{{ $course->info->title }}">
                    <div class="media-body">
                        <h5 class="mt-0">{{ $course->info->title }}</h5>
                        <p>
                            desc: {{ $course->info->description }}
                        </p>
                        <p>
                            created: {{ $course->created_at->format('d-m-Y') }}
                        </p>
                        <p>
                            sub categories:
                            @foreach($course->subCategory as $category)
                                <span>{{$category->title}}</span>
                            @endforeach
                        </p>
                        <p>
                            category: {{ $course->category->title }}
                        </p>
                        <p>
                            language: {{ $course->language->name }}
                        </p>
                        <p>
                            price: {{ $course->price }}
                        </p>
                        <p>
                            limit: {{ $course->limit }}
                        </p>
                        <p>
                            Active: {{ $course->is_active ? 'Yes' : 'No' }}
                        </p>
                    </div>
                </div>
                <div class="my-3">
                    <h3 class="text-center">Content</h3>
                    <div>
                        {!! $course->info->content !!}
                    </div>
                </div>
            </div>
            <div class="col-md-1">
                <button id="delete_course" data-course-id="{{ $course->id }}" class="btn btn-danger">Delete</button>
                <a href="#" class="btn btn-warning">Edit</a>
            </div>
        </div>

    </div>

    <chapters class="mt-3" :course-data="{{ json_encode($course) }}"></chapters>

@endsection

@section('vue')

    <script src="{{ asset('js/vue.js') }}"></script>

@endsection

