@extends('layouts.teacher')

@section('content')
{{--    <form action="{{route('teacher.profile.store')}}" method="POST" enctype="multipart/form-data">--}}
{{--        @csrf--}}
{{--        <div class="row">--}}
{{--            <div class="cart cart-50">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-25 v-center">--}}
{{--                        <div class="form-group @error('photo') is-invalid @enderror">--}}
{{--                            <div class="avatar-upload-btn b-center" id="teacher-photo-container"--}}
{{--                                 onclick="chooseTeacherProfilePhoto();">--}}
{{--                                <img class="default" src="{{asset('img/default_user.svg')}}" alt="">--}}
{{--                                <img class="uploaded" src="" alt="">--}}
{{--                                --}}{{--                                <img class="sm_photo" src="{{asset('img/icons/icons-camera.svg')}}" alt="">--}}
{{--                            </div>--}}
{{--                            @error('photo')--}}
{{--                            <div class="invalid-feedback center">{{ $message }}</div>--}}
{{--                            @enderror--}}

{{--                            <input type="file"--}}
{{--                                   class="form-control-file"--}}
{{--                                   id="teacher-photo"--}}
{{--                                   name="photo"--}}
{{--                                   accept="image/x-png,image/jpg,image/jpeg"--}}
{{--                            >--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-75 v-center">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-50">--}}
{{--                                <div class="form-group @error('first_name') is-invalid @enderror">--}}
{{--                                    <label for="teacher_fname">{{__('profile.first_name')}}</label>--}}
{{--                                    <input id="teacher_fname" name="first_name" class="form-field"--}}
{{--                                           required type="text"--}}
{{--                                           value="{{ old('first_name') }}">--}}

{{--                                    @error('first_name')--}}
{{--                                    <div class="invalid-feedback">{{ $message }}</div>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-50">--}}
{{--                                <div class="form-group @error('last_name') is-invalid @enderror">--}}
{{--                                    <label for="teacher_lname">{{__('profile.last_name')}}</label>--}}
{{--                                    <input type="text" name="last_name" class="form-field"--}}
{{--                                           required id="teacher_lname" value="{{ old('last_name') }}">--}}

{{--                                    @error('last_name')--}}
{{--                                    <div class="invalid-feedback">{{ $message }}</div>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-50">--}}
{{--                                <div class="form-group @error('degree') is-invalid @enderror">--}}
{{--                                    <label for="teacher_degree">{{__('profile.degree')}}</label>--}}
{{--                                    <input type="text" name="degree" class="form-field" required--}}
{{--                                           id="teacher_degree" value="{{ old('degree') }}">--}}

{{--                                    @error('degree')--}}
{{--                                    <div class="invalid-feedback">{{ $message }}</div>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-50">--}}
{{--                                <div class="form-group @error('phone') is-invalid @enderror">--}}
{{--                                    <label for="teacher_phone">{{__('profile.phone')}}</label>--}}
{{--                                    <input id="teacher_phone" name="phone" class="form-field"--}}
{{--                                           type="text" value="{{ old('photo') }}">--}}

{{--                                    @error('phone')--}}
{{--                                    <div class="invalid-feedback">{{ $message }}</div>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-100">--}}
{{--                                <div class="form-group @error('abilities') is-invalid @enderror">--}}
{{--                                    <label>{{__('profile.chose_skills')}}</label>--}}
{{--                                    <select multiple name="abilities[]"--}}
{{--                                            class="@error('abilities') is-invalid @enderror">--}}
{{--                                        @foreach($teacherCategories as $category)--}}
{{--                                            <option--}}
{{--                                                value="{{$category->id}}" {{ (collect(old('abilities'))->contains($category->id)) ? 'selected':'' }}>{{$category->title}}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}

{{--                                    @error('abilities')--}}
{{--                                    <div class="invalid-feedback">{{ $message }}</div>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-100">--}}
{{--                                <div class="form-group">--}}
{{--                                    <input class="styled-checkbox" id="styled-checkbox-1" name="is_public"--}}
{{--                                           type="checkbox">--}}
{{--                                    <label for="styled-checkbox-1">{{__('profile.is_public')}}</label>--}}

{{--                                    @error('is_public')--}}
{{--                                    <div class="invalid-feedback">{{ $message }}</div>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div> --}}{{--End cart--}}

{{--            <div class="cart cart-50">--}}
{{--                <div class="row">--}}
{{--                    @foreach(App\Models\Socialmedia::all() as $item)--}}
{{--                        <div class="form-group col-15 social-icon">--}}
{{--                            <img src="{{asset('img/social_icons/' . $item->title . '.svg')}}" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="form-group col-85">--}}
{{--                            <label for="teacher_{{$item->title}}"> {{$item->title}} </label>--}}
{{--                            <input id="teacher_{{$item->title}}" name="social_{{$item->title}}" class="form-field"--}}
{{--                                   value="{{ old('social_' . $item->title) }}" type="text">--}}
{{--                        </div>--}}
{{--                    @endforeach--}}
{{--                </div>--}}
{{--            </div> --}}{{--End cart--}}
{{--            <div class="cart cart-100">--}}
{{--                <div class="form-group @error('about_me') is-invalid @enderror">--}}
{{--                    <label for="content">{{__('profile.personal_info')}}</label>--}}
{{--                    <div id="create-profile-toolbar-container"></div>--}}

{{--                    <textarea id="about_me_textarea" class="d-none" name="about_me">{{old('about_me')}}</textarea>--}}

{{--                    <div id="create-profile-content"></div>--}}

{{--                    @error('about_me')--}}
{{--                    <div class="invalid-feedback">{{ $message }}</div>--}}
{{--                    @enderror--}}
{{--                </div>--}}

{{--                <div class="form-group text-center b-center">--}}
{{--                    <button type="submit" id="submit_profile_teacher"--}}
{{--                            class="btn b-center">{{__('profile.save_btn')}}</button>--}}
{{--                </div>--}}
{{--            </div> --}}{{--End cart--}}
{{--        </div>--}}
{{--    </form>--}}

    <profile-create></profile-create>

@endsection

@section('vue')
    <script src="{{ asset('js/vue.js') }}"></script>
@endsection


@section('ckeditor')
    <script>

        const inpFile = document.getElementById("teacher-photo");
        const previewContainer = document.getElementById("teacher-photo-container");
        const previewImg = previewContainer.querySelector(".uploaded");
        const defaultImg = previewContainer.querySelector(".default");

        function chooseTeacherProfilePhoto() {
            inpFile.click();
        }

        inpFile.addEventListener("change", function () {
            const file = this.files[0];

            if (file) {
                const reader = new FileReader();

                defaultImg.style.display = "none";
                previewImg.style.display = "flex";

                reader.addEventListener("load", function () {
                    console.log(this);
                    previewImg.setAttribute("src", this.result);
                });

                reader.readAsDataURL(file);
            }
        });
        //
        // ClassicEditor //ClassicEditor //InlineEditor // BalloonEditor // DecoupledEditor
        //     .create( document.querySelector( '#create-profile-content' ) )
        //     .then( editor => {
        //         console.log( editor );
        //     } )
        //     .catch( error => {
        //         console.error( error );
        //     } );

        DecoupledEditor
            .create(document.querySelector('#create-profile-content'))
            .then(editor => {
                const toolbarContainer = document.querySelector('#create-profile-toolbar-container');

                window.editor = editor;
                toolbarContainer.appendChild(editor.ui.view.toolbar.element);

                let textarea = document.getElementById("about_me_textarea");
                editor.setData(textarea.value);
            })
            .catch(error => {
                console.error(error);
            });

        document.getElementById('submit_profile_teacher').onclick = () => {
            document.getElementById('about_me_textarea').value = editor.getData();
        }

    </script>
@endsection

