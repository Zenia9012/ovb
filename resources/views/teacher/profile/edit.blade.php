@extends('layouts.teacher')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Teacher profile</div>

                    <div class="card-body">
                        <form action="{{route('teacher.profile.update')}}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="teacher-first-name">First name</label>
                                    <input type="text" class="form-control" id="teacher-first-name" name="first_name" value="{{$profile->first_name}}" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="teacher-last-name">Last name</label>
                                    <input type="text" class="form-control" id="teacher-last-name" name="last_name" value="{{$profile->last_name}}" required>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="teacher-degree">Degree</label>
                                    <input type="text" class="form-control" id="teacher-degree" name="degree" value="{{$profile->degree}}" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="teacher-phone">Phone</label>
                                    <input type="text" class="form-control" id="teacher-phone" name="phone" value="{{$profile->phone}}" required>
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <button type="submit" class="btn btn-primary">Change</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

