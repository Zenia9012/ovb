@extends('layouts.teacher')
@section('bootstrap')
    @include('bootstrap')
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <individual-lessons-list
                    :lessons-data="{{ json_encode($lessons) }}"
                    :teacher-groups-data="{{ json_encode($groups) }}"
                    :teacher-students-data="{{ json_encode($students) }}">
                </individual-lessons-list>
            </div>
        </div>
    </div>
@endsection
@section('vue')

    <script src="{{ asset('js/vue.js') }}"></script>

@endsection

