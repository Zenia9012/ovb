@extends('layouts.teacher')
@section('bootstrap')
    @include('bootstrap')
@endsection
@section('content')

    <individual-lessons-edit
        :lesson-data="{{ json_encode($lesson) }}"
        :students-data="{{ json_encode($students) }}">
    </individual-lessons-edit>

@endsection
@section('vue')

    <script src="{{ asset('js/vue.js') }}"></script>

@endsection

