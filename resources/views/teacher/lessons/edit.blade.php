@extends('layouts.teacher')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <form method="post" action="{{ route('teacher.lessons.update', [$course->id, $chapter->id, $lesson->id]) }}">
                    @csrf
                    @method('PUT')

                    <div class="form-group">
                        <p>Course: {{ $course->info->title }}</p>
                    </div>
                    <div class="form-group">
                        <p>Chapter: {{ $chapter->title }}</p>
                    </div>


                    <div class="form-group">
                        <label for="title">{{ __('courses.add_lesson_title') }}</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title"
                               name="title" value="{{ $lesson->title }}">

                        @error('title')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="description">{{ __('courses.add_lesson_desc') }}</label>
                        <textarea class="form-control @error('description') is-invalid @enderror" id="description"
                                  rows="3" name="description">{{ $lesson->description }}</textarea>

                        @error('description')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="content">{{ __('courses.add_lesson_content') }}</label>
                        <textarea class="form-control" id="edit-lesson-content" rows="3"
                                  name="content">{{ $lesson->content }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="duration">{{ __('courses.add_lesson_duration') }}</label>
                        <input type="number" step="0.01" class="form-control @error('duration') is-invalid @enderror"
                               id="duration" name="duration" value="{{ $lesson->duration }}">

                        @error('duration')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('ckeditor')
    <script>
        CKEDITOR.replace('edit-lesson-content');
    </script>
@endsection

