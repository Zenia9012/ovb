@extends('layouts.teacher')
@section('bootstrap')
    @include('bootstrap')
@endsection
@section('content')
    <teacher-groups-list :groups-data="{{ $groups }}" :clients-data="{{ $clients }}"></teacher-groups-list>
@endsection
@section('vue')

    <script src="{{ asset('js/vue.js') }}"></script>

@endsection

