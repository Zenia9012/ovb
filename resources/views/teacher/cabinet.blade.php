@extends('layouts.teacher')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Teacher cabinet</div>

                <ul>
                    <li><a href="{{route('teacher.profile.index')}}">profile</a></li>
                    <li><a href="{{ route('teacher.courses.my-courses') }}">My courses</a></li>
                    <li><a href="{{route('teacher.courses.index')}}">All Courses</a></li>
                    <li><a href="{{route('teacher.invite.index')}}">Invite Student</a></li>
                    <li><a href="{{route('teacher.students.requests.index')}}">Requests Student</a></li>
                    <li>
                        Add telegram @include('includes.telegram_login')
                    </li>
                </ul>

            </div>
        </div>
    </div>
</div>
@endsection

