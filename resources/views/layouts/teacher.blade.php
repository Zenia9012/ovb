<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('css/teacher.css') }}" rel="stylesheet">

    <script src="https://kit.fontawesome.com/f97efe7d35.js" crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}"></script>
{{--    <script src="{{ asset('js/ckeditor5-build-classic/ckeditor.js') }}"></script>--}}
    <script src="{{ asset('js/ckeditor5-build-decoupled-document/ckeditor.js') }}"></script>

    @yield('bootstrap')
</head>
<body>
<div id="app">
    <header>

        @include('includes.notify')

        <a class="header-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>

        <div id="navbar">

            <div class="navbar-link @routeis('teacher.cabinet.*') active @endrouteis">
                <a href="{{route('teacher.cabinet.index')}}">{{__('menu.home')}}</a>
            </div>

            {{--                        <div class="navbar-link @routeis('teacher.profile.*') active @endrouteis">--}}
            {{--                            <a href="{{route('teacher.profile.index')}}">--}}
            {{--                                <span>Профиль</span>--}}
            {{--                            </a>--}}
            {{--                        </div>--}}

            <div class="navbar-link">
                <a href="">{{__('menu.students')}}</a>
            </div>

            <div class="navbar-link">
                <a href="">{{__('menu.lessons')}}</a>
            </div>


        </div>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    @if (Route::has('register'))
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    @endif
                @else
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        @csrf
                    </form>
                @endguest
            </ul>
        </div>
    </header>

    <main>
{{--        <nav>--}}
{{--            <div class="border-bottom">--}}
{{--                <div class="main-container">--}}
{{--                    --}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </nav>--}}



        <div id="content">
            <div class="main-container">
                @yield('content')
            </div>
        </div>
    </main>
</div>

@yield('ckeditor')

<!-- Scripts -->
@yield('vue')
<script>
    const lang = '<?php echo config('app.locale'); ?>'
</script>
<script src="{{ mix('js/teacher.js') }}"></script>

</body>
</html>
