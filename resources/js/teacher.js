jQuery(function ($) {

    'use strict';

    $('#category').change(function () {
        let category = $(this).val();

        $.ajax({
            type: "GET",
            url: '/api/courses/sub-category',
            data: {
                'category': category,
            },
            dataType: 'json',
            success: function (result) {

                let str = '';
                result.forEach(function (category) {
                    str += `<option value='${category.id}'>${category.title}</option>`
                });

                $('#sub_category').html(str);
            }
        });
    });

    $('#add-chapter').on('shown.bs.modal', function () {
        $('#add-chapter-title').focus();
    });

    $('#delete_course').click(function () {
        const courseId = $(this).data('course-id');
        const sure = confirm('Are you sure');
        if (sure) {
            axios.delete('/api/courses/' + courseId)
                .then(response => {
                    if (response.data === true) {
                        location.replace('/teachers/courses/my-courses')
                    }
                }).catch(error => {
                    alert(error.response.statusText);
            });
        }
    })

});
