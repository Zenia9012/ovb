export default {
    text: {
        templateTitle: 'Text',
        id: 1,
        templateIcon: '',
        classComponent: 'component-text',
        tags:
            [
                {
                    type: 'div',
                    class: 'template-text1',
                    text: 'Example text'
                },
            ],
    },
    image: {
        templateTitle: 'Image',
        id: 2,
        templateIcon: '',
        classComponent: 'component-image',
        tags:
            [
                {
                    type: 'img',
                    src: '',
                    class: 'template-text',
                    text: 'Example image'
                },
            ],
    }
}
