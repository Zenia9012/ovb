/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


window.Vue = require('vue');
import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated';

Vue.use(VueInternationalization);

const lang = document.documentElement.lang.substr(0, 2);
console.log(lang);

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// teachers
Vue.component('chapters', require('./components/ChaptersComponent.vue').default);
Vue.component('create-private-lesson', require('./components/teacher/lessons/CreateLessonComponent.vue').default);
Vue.component('student-requests', require('./components/teacher/students_requests/ListStudentRequestsComponent.vue').default);
Vue.component('courses-list', require('./components/CoursesListComponent.vue').default);
Vue.component('individual-lessons-list', require('./components/IndividualLessonsListComponent.vue').default);
Vue.component('individual-lessons-edit', require('./components/IndividualLessonsEditComponent.vue').default);
Vue.component('teacher-groups-list', require('./components/TeacherGroupListComponent.vue').default);
Vue.component('invite-page', require('./components/teacher/invite/InvitePageComponent.vue').default);

Vue.component('profile-create', require('./components/teacher/profile/CreateComponent.vue').default);

//clients
Vue.component('teachers-list', require('./components/clients/TeacherListComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    i18n
});
