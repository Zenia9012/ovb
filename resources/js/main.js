jQuery(document).ready(function($){

    let speed = 250;
    $('.info-block .main').mouseover(function () {
        $(this).parent().addClass('shoving');
        $(this).hide(speed);
        $(this).parent().find('.info-content').show(speed, function () {
            $(this).parent().removeClass('shoving');
        });
    });

    $('.info-block').mouseleave(function () {
        if(!$(this).hasClass('shoving')){
            $(this).find('.main').show(speed);
            $(this).find('.info-content').hide(speed);
        }
    });



    // Custom multiple select
    let select = $('select[multiple]');
    let options = select.find('option');

    let div = $('<div />').addClass('selectMultiple');
    if(select.hasClass('is-invalid')){
        div.addClass('is-invalid');
    }
    let active = $('<div />');
    let list = $('<ul />');
    let placeholder = select.data('placeholder');

    let span = $('<span />').text(placeholder).appendTo(active);

    options.each(function() {
        let text = $(this).text();
        if($(this).is(':selected')) {
            active.append($('<a />').html('<em>' + text + '</em><i></i>'));
            span.addClass('hide');
        } else {
            list.append($('<li />').html(text));
        }
    });

    active.append($('<div />').addClass('arrow'));
    div.append(active).append(list);

    select.wrap(div);

    $(document).on('click', '.selectMultiple ul li', function(e) {
        let select = $(this).parent().parent();
        let selectedLength = select.children('div').children('a').length;
        if( selectedLength <= 2 && selectedLength >= 0) {
            let li = $(this);
            if (!select.hasClass('clicked')) {
                select.addClass('clicked');
                li.prev().addClass('beforeRemove');
                li.next().addClass('afterRemove');
                li.addClass('remove');
                let a = $('<a />').addClass('notShown').html('<em>' + li.text() + '</em><i></i>').hide().appendTo(select.children('div'));
                a.slideDown(200, function () {
                    setTimeout(function () {
                        a.addClass('shown');
                        select.children('div').children('span').addClass('hide');
                        select.find('option:contains(' + li.text() + ')').prop('selected', true);
                    }, 240);
                });
                setTimeout(function () {
                    if (li.prev().is(':last-child')) {
                        li.prev().removeClass('beforeRemove');
                    }
                    if (li.next().is(':first-child')) {
                        li.next().removeClass('afterRemove');
                    }
                    setTimeout(function () {
                        li.prev().removeClass('beforeRemove');
                        li.next().removeClass('afterRemove');
                    }, 100);

                    li.slideUp(400, function () {
                        li.remove();
                        select.removeClass('clicked');
                    });
                }, 100);
            }
        }
    });

    $(document).on('click', '.selectMultiple > div a', function(e) {
        let select = $(this).parent().parent();
        let self = $(this);
        self.removeClass().addClass('remove');
        select.addClass('open');
        setTimeout(function() {
            self.addClass('disappear');
            setTimeout(function() {
                self.animate({
                    width: 0,
                    height: 0,
                    padding: 0,
                    margin: 0
                }, 300, function() {
                    let li = $('<li />').text(self.children('em').text()).addClass('notShown').appendTo(select.find('ul'));
                    li.slideDown(400, function() {
                        li.addClass('show');
                        setTimeout(function() {
                            select.find('option:contains(' + self.children('em').text() + ')').prop('selected', false);
                            if(!select.find('option:selected').length) {
                                select.children('div').children('span').removeClass('hide');
                            }
                            li.removeClass();
                        }, 400);
                    });
                    self.remove();
                })
            }, 300);
        }, 400);
    });

    $(document).on('click', '.selectMultiple > div .arrow, .selectMultiple > div span', function(e) {
        $(this).parent().parent().toggleClass('open');
    });
// end custom multiple select

    //notify message
    $('.notify-message').fadeOut(6000);
    // end notify message

    // types is "error" and "success"
    window.notify = (message, type = 'success') => {
        const notifyBlock = $('#js-notify-message-' + type);

        let messageDiv = $('.notify-message-text-' + type);
        messageDiv.text(message)

        notifyBlock.show();
        notifyBlock.fadeOut(6000, function () {
            messageDiv.text()
        });
    }

    window.makeSure = (message, callback) => {
        const notifyBlock = $('#js-notify-make-sure');
        $('.modal-backdrop').show();

        let messageDiv = $('.notify-message-text');
        messageDiv.text(message)

        notifyBlock.show();

        $('#make-sure-okay, #make-sure-cancel').click(function () {
            if (this.id == 'make-sure-okay') {
                clickOkay();
                notifyBlock.hide();
                $('.modal-backdrop').hide();
            }
            else if (this.id == 'make-sure-cancel') {
                clickCancel()
                notifyBlock.hide();
                $('.modal-backdrop').hide();
            }
        });

        function clickOkay() {
            callback(true);
        }
        function clickCancel() {
            callback(false);
        }
    }
});
