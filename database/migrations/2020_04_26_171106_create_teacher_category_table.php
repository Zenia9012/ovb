<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_category', function (Blueprint $table) {
            $table->id();

            $table->foreignId('teacher_profile_id');
            $table->foreign('teacher_profile_id')->references('id')->on('teacher_profiles')->cascadeOnDelete();

            $table->foreignId('teacher_category_id');
            $table->foreign('teacher_category_id')->references('id')->on('teacher_categories')->cascadeOnDelete();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_category');
    }
}
