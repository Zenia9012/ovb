<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialmediaUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socialmedia_users', function (Blueprint $table) {
            $table->id();

            $table->foreignId('profile_id');
            $table->foreign('profile_id')->references('id')->on('teacher_profiles')->cascadeOnDelete();

            $table->foreignId('socialmedia_id');
            $table->foreign('socialmedia_id')->references('id')->on('socialmedia')->cascadeOnDelete();

            $table->string('link');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socialmedia_users');
    }
}
