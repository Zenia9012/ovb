<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_lessons', function (Blueprint $table) {
            $table->id();
            $table->foreignId('client_teacher_id');
            $table->foreign('client_teacher_id')->references('id')->on('client_teachers')->cascadeOnDelete();
            $table->foreignId('lesson_id');
            $table->foreign('lesson_id')->references('id')->on('individual_lessons')->cascadeOnDelete();
            $table->boolean('is_completed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_lessons');
    }
}
