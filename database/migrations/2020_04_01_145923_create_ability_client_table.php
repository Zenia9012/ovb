<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAbilityClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ability_clients', function (Blueprint $table) {
            $table->id();

            $table->foreignId('client_id');
            $table->foreign('client_id')->references('id')->on('users')->cascadeOnDelete();

            $table->foreignId('course_categories_id');
            $table->foreign('course_categories_id')->references('id')->on('course_categories')->cascadeOnDelete();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ability_client');
    }
}
