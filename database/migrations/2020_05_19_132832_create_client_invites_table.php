<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_invites', function (Blueprint $table) {
            $table->id();
            $table->foreignId('teacher_id');
            $table->foreign('teacher_id')->references('id')->on('users')->cascadeOnDelete();
            $table->foreignId('client_id')->nullable();
            $table->foreign('client_id')->references('id')->on('users')->cascadeOnDelete();
            $table->boolean('is_accepted')->default(0);
            $table->string('token',15)->nullable();
            $table->string('email',100)->nullable();
            $table->timestamps();

            $table->unique(['teacher_id', 'client_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_invites');
    }
}
