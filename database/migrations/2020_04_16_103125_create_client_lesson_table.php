<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientLessonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_lesson', function (Blueprint $table) {
            $table->id();
            $table->integer('client_group_id')->unsigned();
            $table->string('client_group_type');
            $table->integer('lesson_theme_id')->unsigned();
            $table->string('lesson_theme_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_lesson');
    }
}
