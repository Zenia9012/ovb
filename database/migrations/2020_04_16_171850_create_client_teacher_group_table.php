<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientTeacherGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_teacher_group', function (Blueprint $table) {
            $table->id();
            $table->foreignId('group_id');
            $table->foreign('group_id')->references('id')->on('teacher_groups')->cascadeOnDelete();
            $table->foreignId('client_teacher_id');
            $table->foreign('client_teacher_id')->references('id')->on('client_teachers')->cascadeOnDelete();
            $table->timestamps();

            $table->unique(['group_id', 'client_teacher_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_teacher_group');
    }
}
