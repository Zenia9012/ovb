<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $now = Carbon::now();

        // admin user
        DB::table('users')->insert([[
            'email' => 'admin@gmail.com',
            'password' => bcrypt('12341234'),
            'remember_token' => str_random(10),
            'is_admin' => true,
            'email_verified_at' => $now,
            'created_at' => $now,
            'updated_at' => $now,
        ]]);
        $lastId = DB::getPDO()->lastInsertId();

        DB::table('client_profiles')->insert([[
            'client_id' => $lastId,
            'first_name' => 'Admin',
            'last_name' => 'Admin',
        ]]);

        // client user
        DB::table('users')->insert([[
            'email' => 'client@gmail.com',
            'password' => bcrypt('12341234'),
            'remember_token' => str_random(10),
            'email_verified_at' => $now,
            'created_at' => $now,
            'updated_at' => $now,
        ]]);
        $lastId = DB::getPDO()->lastInsertId();

        DB::table('client_profiles')->insert([[
            'client_id' => $lastId,
            'first_name' => 'Client',
            'last_name' => 'Client',
        ]]);

        // teacher user
        DB::table('users')->insert([[
            'email' => 'teacher@gmail.com',
            'password' => bcrypt('12341234'),
            'remember_token' => str_random(10),
            'email_verified_at' => $now,
            'is_teacher' => true,
            'created_at' => $now,
            'updated_at' => $now,
        ]]);
        $lastId = DB::getPDO()->lastInsertId();

        DB::table('teacher_profiles')->insert([[
            'teacher_id' => $lastId,
            'first_name' => 'Teacher',
            'last_name' => 'Teacher',
            'degree' => $faker->text(50),
            'about_me' => $faker->randomHtml(),
            'photo' => 'img/default_user.svg',
        ]]);

        factory(User::class, 30)->create()->each(function ($user) use ($faker){
            if ($user->is_teacher){
                DB::table('teacher_profiles')->insert([[
                    'teacher_id' => $user->id,
                    'first_name' => $faker->firstName,
                    'last_name' => $faker->lastName,
                    'degree' => $faker->text(50),
                    'about_me' => $faker->text(100),
                    'photo' => 'img/default_user.svg',
                    'is_public' => $faker->boolean(70),
                ]]);

            }else{
                DB::table('client_profiles')->insert([[
                    'client_id' => $user->id,
                    'first_name' => $faker->firstName,
                    'last_name' => $faker->lastName,
                ]]);
            }
        });
    }
}
