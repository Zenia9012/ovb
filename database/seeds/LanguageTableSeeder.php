<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class LanguageTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('languages')->insert([
            [
                'name' => 'Ukraine',
                'iso_code' => 'uk',
            ],
            [
                'name' => 'English',
                'iso_code' => 'en',
            ],
        ]);
    }
}
