<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Illuminate\Support\Facades\DB;
use Laracasts\TestDummy\Factory as TestDummy;

class SocialmediaTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('socialmedia')->insert([
            [
                'title' => 'Facebook',
                'icon' => '',
            ],
            [
                'title' => 'YouTube',
                'icon' => '',
            ],
            [
                'title' => 'Instagram',
                'icon' => '',
            ],
            [
                'title' => 'LinkedIn',
                'icon' => '',
            ],
            [
                'title' => 'Twitter',
                'icon' => '',
            ],
        ]);
    }
}
