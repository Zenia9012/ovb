<?php

use Illuminate\Database\Seeder;


class LessonTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Models\Lesson::class, 100)->create();
    }
}
