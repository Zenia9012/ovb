<?php

use Illuminate\Database\Seeder;


class ChapterTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Models\Chapter::class, 30)->create();
    }
}
