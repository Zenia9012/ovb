<?php

use Illuminate\Database\Seeder;
use App\Models\TeacherRequests;

class TeacherRequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(TeacherRequests::class, 30)->create();
    }
}
