<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CourseCategoriesTableSeeder::class);
        $this->call(CourseSubCategoryTableSeeder::class);
        $this->call(LanguageTableSeeder::class);
        $this->call(CourseInfoTableSeeder::class);
        $this->call(CourseTableSeeder::class);
        $this->call(CourseUserTableSeeder::class);
        $this->call(ChapterTableSeeder::class);
        $this->call(LessonTableSeeder::class);
        $this->call(SocialmediaTableSeeder::class);
        $this->call(CourseBookmarksTableSeeder::class);
        $this->call(ClientTeacherTableSeeder::class);
        $this->call(IndividualLessonThemeTableSeeder::class);
        $this->call(IndividualLessonsTableSeeder::class);
        $this->call(TeacherGroupTableSeeder::class);
        $this->call(TeacherCategoriesTableSeeder::class);
        $this->call(TeacherRequestsTableSeeder::class);
        $this->call(StatusLessonTableSeeder::class);
        $this->call(ClientInvitesTableSeeder::class);
    }
}
