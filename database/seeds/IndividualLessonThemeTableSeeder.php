<?php

use Illuminate\Database\Seeder;

class IndividualLessonThemeTableSeeder extends Seeder
{
    const INDIVIDUAL_LESSON_MAX_SEED = 10;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\IndividualLessonTheme::class, self::INDIVIDUAL_LESSON_MAX_SEED)->create();
    }
}
