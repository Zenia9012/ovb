<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CourseTableSeeder extends Seeder {

    public function run()
    {
        factory(\App\Models\Course::class, 20)->create()->each(function ($item) {
            $iteration = rand(1,3);
            for ($i = 0; $i < $iteration; $i++){
                DB::table('course_sub_category')->insert([
                    'sub_category_id' => \App\Models\CourseSubCategory::all()->random()->id,
                    'course_id' => $item->id,
                ]);
            }
        });


    }
}
