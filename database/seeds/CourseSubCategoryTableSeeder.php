<?php

use Illuminate\Database\Seeder;

class CourseSubCategoryTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Models\CourseSubCategory::class, 40)->create();
    }
}
