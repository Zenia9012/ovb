<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class IndividualLessonsTableSeeder extends Seeder {

    protected $clientTypes = [\App\Models\ClientTeacher::class, \App\Models\TeacherGroup::class];
    protected $lessonTypes = [\App\Models\IndividualLesson::class, \App\Models\IndividualLessonTheme::class];

    public function run()
    {
        factory(\App\Models\IndividualLesson::class, 20)->create()->each(function ($lesson) {
            DB::table('client_lesson')->insert([
                'client_group_id' => rand(1, 10),
                'client_group_type' => $this->clientTypes[rand(0, 1)],
                'lesson_theme_id' => rand(1, 10),
                'lesson_theme_type' => $this->lessonTypes[rand(0, 1)],
            ]);
        });
    }
}
