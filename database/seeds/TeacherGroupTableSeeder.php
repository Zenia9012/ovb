<?php

use App\Models\TeacherGroup;
use App\Models\ClientTeacher;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class TeacherGroupTableSeeder extends Seeder {

    public function run()
    {
        DB::table('teacher_groups')->insert([
            'teacher_id' => 3,
            'title' => 'Test group',
        ]);

        factory(TeacherGroup::class, 10)->create()->each(function ($group) {
            DB::table('client_teacher_group')->insert([
                'group_id' => $group->id,
                'client_teacher_id' => ClientTeacher::all()->random()->id,
            ]);
        });

        DB::table('client_teacher_group')->insert([
            'group_id' => 1,
            'client_teacher_id' => 1,
        ]);

        DB::table('client_teacher_group')->insert([
            'group_id' => 1,
            'client_teacher_id' => 2,
        ]);
    }
}
