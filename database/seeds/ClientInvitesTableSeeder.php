<?php

use App\Models\ClientInvite;
use Illuminate\Database\Seeder;

class ClientInvitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ClientInvite::class, 1)->create();
    }
}
