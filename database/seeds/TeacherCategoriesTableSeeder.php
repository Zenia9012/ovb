<?php

use App\Models\TeacherProfile;
use Illuminate\Database\Seeder;
use App\Models\TeacherCategory;
use Illuminate\Support\Facades\DB;

class TeacherCategoriesTableSeeder extends Seeder {

    public function run()
    {
        $teachers = TeacherProfile::all();
        factory(TeacherCategory::class, 10)
            ->create()
            ->each(function ($category) use ($teachers) {
                DB::table('teacher_category')->insert([[
                    'teacher_profile_id' => $teachers->random()->id,
                    'teacher_category_id' => $category->id,
                ]]);
            });
    }
}
