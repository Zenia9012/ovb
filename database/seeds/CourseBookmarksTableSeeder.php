<?php

use Illuminate\Database\Seeder;

class CourseBookmarksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\CourseBookmark::class, 20)->create();
    }
}
