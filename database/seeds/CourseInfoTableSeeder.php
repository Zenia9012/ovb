<?php

use Illuminate\Database\Seeder;


class CourseInfoTableSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Models\CourseInfo::class, 20)->create();
    }
}
