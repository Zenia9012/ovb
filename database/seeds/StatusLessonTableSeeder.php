<?php

use App\Models\StatusLesson;
use Illuminate\Database\Seeder;

class StatusLessonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(StatusLesson::class, 10)->create();
    }
}
