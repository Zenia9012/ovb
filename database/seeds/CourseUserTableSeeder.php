<?php

use App\User;
use App\Models\Course;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseUserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $now = \Carbon\Carbon::now();
        $courser = Course::all('id');

        for ($i = 0; $i < $courser->count(); $i++) {
            DB::table('course_user')->insert([
                [
                    'client_id' => $faker->numberBetween(1, 2),
                    'course_id' => $courser->get($i)->id,
                    'created_at' => $now,
                    'updated_at' => $now,
                ],
            ]);
        }


    }
}
