<?php

use App\Models\ClientTeacher;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientTeacherTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('client_teachers')->insert([[
            'client_id' => 1,
            'teacher_id' => 3,
            'is_active' => 1,
            'is_verified' => 1,
        ], [
            'client_id' => 2,
            'teacher_id' => 3,
            'is_active' => 1,
            'is_verified' => 1,
        ],
        ]);
        factory(ClientTeacher::class, 10)->create();
    }
}
