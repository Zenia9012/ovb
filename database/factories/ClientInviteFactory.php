<?php

/** @var Factory $factory */

use App\User;
use Faker\Generator as Faker;
use App\Models\ClientInvite;
use Illuminate\Database\Eloquent\Factory;

$factory->define(ClientInvite::class, function (Faker $faker) {
    return [
        'teacher_id' => User::getTeachers()->random()->id,
        'client_id' => User::getClients()->random()->id,
        'is_accepted' => $faker->boolean,
    ];
});
