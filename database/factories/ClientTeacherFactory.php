<?php

/** @var Factory $factory */

use App\User;
use Faker\Generator as Faker;
use App\Models\ClientTeacher;
use Illuminate\Database\Eloquent\Factory;


$factory->define(ClientTeacher::class, function (Faker $faker) {
    return [
        'client_id' => User::getClients()->random()->id,
        'teacher_id' => User::getTeachers()->random()->id,
        'is_active' => $faker->boolean,
        'is_verified' => $faker->boolean,
    ];
});
