<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use App\Models\CourseCategory;
use Illuminate\Database\Eloquent\Factory;

$factory->define(CourseCategory::class, function (Faker $faker) {
    return [
        'title' => $faker->word
    ];
});
