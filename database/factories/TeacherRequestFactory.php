<?php

/** @var Factory $factory */

use App\User;
use Faker\Generator as Faker;
use App\Models\TeacherRequests;
use Illuminate\Database\Eloquent\Factory;


$factory->define(TeacherRequests::class, function (Faker $faker) {
    return [
        'message' => $faker->randomElement(['', $faker->text(40)]),
        'teacher_id' => User::getTeachers()->random()->id,
        'client_id' => User::getClients()->random()->id,
        'is_accepted' => $faker->randomElement([null, 0, 1]),
    ];
});
