<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use App\Models\StatusLesson;
use Illuminate\Database\Eloquent\Factory;

$factory->define(StatusLesson::class, function (Faker $faker) {
    return [
        'client_teacher_id' => rand(1,10),
        'lesson_id' => rand(1,20),
        'is_completed' => 1,
    ];
});
