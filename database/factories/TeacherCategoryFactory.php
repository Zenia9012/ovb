<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\TeacherCategory::class, function (Faker $faker) {
    return [
        'title' => $faker->word
    ];
});
