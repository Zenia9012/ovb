<?php

/** @var Factory $factory */

use App\User;
use Faker\Generator as Faker;
use App\Models\IndividualLessonTheme;
use Illuminate\Database\Eloquent\Factory;

$factory->define(IndividualLessonTheme::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50),
        'teacher_id' => User::getTeachers()->random()->id,
    ];
});
