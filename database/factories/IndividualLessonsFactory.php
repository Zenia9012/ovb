<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\IndividualLesson;
use Faker\Generator as Faker;

$factory->define(IndividualLesson::class, function (Faker $faker) {
    return [
        'teacher_id' => \App\User::getTeachers()->random()->id,
        'theme_id' => $faker->randomElement([null, rand(1, IndividualLessonThemeTableSeeder::INDIVIDUAL_LESSON_MAX_SEED)]),
        'title' => $faker->text(15),
        'description' => $faker->text(50),
        'is_active' => $faker->boolean,
    ];
});
