<?php

/** @var Factory $factory */

use App\User;
use App\Models\Course;
use Faker\Generator as Faker;
use App\Models\CourseBookmark;
use Illuminate\Database\Eloquent\Factory;

$factory->define(CourseBookmark::class, function (Faker $faker) {
    return [
        'course_id' => Course::all()->random()->id,
        'client_id' => User::getClients()->random()->id,
    ];
});
