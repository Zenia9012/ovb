<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Lesson;
use App\Models\Chapter;
use Faker\Generator as Faker;

$factory->define(Lesson::class, function (Faker $faker) {
    $count = Chapter::all()->count();
    return [
        'chapter_id' => rand(1, $count),
        'title' => $faker->text(rand(20,50)),
        'description' => $faker->text(rand(120,250)),
        'display_order' => rand(1,100),
    ];
});
