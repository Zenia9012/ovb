<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Models\TeacherGroup;

$factory->define(TeacherGroup::class, function (Faker $faker) {
    return [
        'teacher_id' => \App\User::getTeachers()->random()->id,
        'title' => $faker->text(30),
    ];
});
