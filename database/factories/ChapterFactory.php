<?php

/** @var Factory $factory */

use App\Models\Course;
use App\Models\Chapter;
use Faker\Generator as Faker;

$factory->define(Chapter::class, function (Faker $faker) {
    return [
        'course_id' => Course::all()->random()->id,
        'title' => $faker->text(rand(10,100)),
        'description' => $faker->text(rand(50,150)),
        'display_order' => rand(1,10),
    ];
});
