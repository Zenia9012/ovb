<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'teacher_id' => \App\User::all('id')->random()->id,
        'language_id' => \App\Models\Language::all('id')->random()->id,
        'info_id' => \App\Models\CourseInfo::all('id')->random()->id,
        'price' => $faker->randomElement([0, 9, 19, 29, 39]),
        'limit' => $faker->numberBetween(1,10),
    ];
});
