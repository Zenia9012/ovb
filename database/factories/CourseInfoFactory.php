<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CourseInfo;
use Faker\Generator as Faker;

$factory->define(CourseInfo::class, function (Faker $faker) {
    return [
        'title' => $faker->text(20),
        'description' => $faker->text(50),
        'content' => $faker->text(100),
    ];
});
