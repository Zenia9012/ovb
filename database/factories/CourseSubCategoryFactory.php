<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CourseSubCategory;
use Faker\Generator as Faker;

$factory->define(CourseSubCategory::class, function (Faker $faker) {
    return [
        'category_id' => \App\Models\CourseCategory::all('id')->random()->id,
        'title' => $faker->word,
    ];
});
