<?php

namespace App\Telegram;


use App\User;

class TelegramMessages {

    /**
     * @var TelegramCore
     */
    private $telegram;

    /**
     * TelegramMessages constructor.
     */
    public function __construct()
    {
        $this->telegram = new TelegramCore();
    }

    /**
     * send single message
     *
     * @param              $message
     * @param array|string $recipient
     */
    public function message($message, $recipient)
    {
        if (is_array($recipient)) {
            foreach ($recipient as $user) {
                $this->telegram->send($message, $user);
            }
        }else{
            $this->telegram->send($message, $recipient);
        }
    }

    /**
     * send multiple message
     *
     * @param array        $messages
     * @param array|string $recipient
     * @return void
     */
    public function messages(array $messages, $recipient)
    {
        foreach ($messages as $message) {
            $this->message($message, $recipient);
        }
    }

    /**
     * debug message
     * @param $message
     */
    public function messageDev($message)
    {
        if (is_array($message)){
            $this->messages($message, config('services.telegram.debug'));
        }else{
            $this->message($message, config('services.telegram.debug'));
        }
    }

    /**
     * message to admin
     * @param $message
     */
    public function messageAdmin($message)
    {
        $admins = User::getAdmins();

        if (is_array($message)){
            $this->messages($message, $admins->pluck('telegram_id'));
        }else{
            $this->message($message, $admins->pluck('telegram_id'));
        }
    }
}
