<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model {

    protected $fillable = ['user_id', 'provider_user_id', 'provider'];

    /**
     * relation to users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * create user and social account user if it not exists
     *
     * @param      $providerUser
     * @param      $providerType
     * @param bool $isTeacher
     * @return mixed
     */
    public static function createOrGetUser($providerUser, $providerType, $isTeacher = false, $email = null)
    {
        $account = SocialAccount::whereProvider($providerType)
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if ($account) {
            return $account->user;
        } else {
            if ($email == null){
                $email = $providerUser->getEmail();
            }
            if ($email == null){
                return false;
            }
            $account = new self([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $providerType,
            ]);
            $user = User::whereEmail($email)->first();
            if (!$user) {
                $user = User::create([
                    'email' => $email,
                    'password' => md5(rand(1, 10000)),
                    'email_verified_at' => Carbon::now(),
                    'is_teacher' => $isTeacher,
                ]);
                if ($isTeacher){
                    TeacherProfile::create([
                        'teacher_id' => $user->id,
                        'first_name' => $providerUser->getName(),
                    ]);
                }else{
                    ClientProfile::create([
                        'client_id' => $user->id,
                        'first_name' => $providerUser->getName(),
                    ]);
                }
            }
            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }
}
