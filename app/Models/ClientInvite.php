<?php

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ClientInvite extends AbstractModels {

    /**
     * get invites by teacher id with relations
     *
     * @param $id
     * @return Builder[]|Collection
     */
    public static function getByTeacher($id)
    {
        return self::with('client.clientProfile')->where('teacher_id', $id)->get();
    }

    /**
     * relation to user
     *
     * @return BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(User::class, 'client_id');
    }

    /**
     * get invite by token
     * @param $token
     * @return mixed
     */
    public static function getByToken($token)
    {
        return self::where('token', $token)->first();
    }
}
