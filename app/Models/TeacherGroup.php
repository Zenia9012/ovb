<?php

namespace App\Models;


use App\Traits\TeacherModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class TeacherGroup extends AbstractModels {

    use TeacherModelTrait;

    /**
     * get groups by ids
     *
     * @param array $ids
     * @return Builder|Builder[]|Collection|Model|null
     */
    public static function getGroupsByIds(array $ids)
    {
        return self::with('clients')->find($ids);
    }

    /**
     * get groups by teacher id with clients info
     *
     * @param $teacherId
     * @return Builder[]|Collection
     */
    public static function getTeachersGroupsWithClientInfo($teacherId)
    {
        return self::with(['clients' => function ($client) {
            $client->with(['user' => function ($item) {
                $item->with('clientProfile');
            }]);
        }])->where('teacher_id', $teacherId)->get();
    }

    /**
     * polymorphic relation to lesson
     */
    public function clientsToLessons()
    {
        return $this->morphedByMany(ClientTeacher::class, 'client_group', 'client_lesson', 'client_group_id', 'id');
    }

    /**
     * relation to client_teachers table
     * pivot table client_teacher_group
     * many to many
     * @return BelongsToMany
     */
    public function clients()
    {
        return $this->belongsToMany(ClientTeacher::class, 'client_teacher_group', 'group_id', 'client_teacher_id');
    }
}
