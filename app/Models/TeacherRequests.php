<?php

namespace App\Models;


use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TeacherRequests extends AbstractModels
{

    /**
     * @param $clientId
     * @return mixed
     */
    public static function getRequestsByClientId($clientId)
    {
        return self::where('client_id', $clientId)->where('is_accepted', 0)->get();
    }

    /**
     * get requests with client and profile by teacher id and is not accepted or declined
     * @param $teacherId
     * @return mixed
     */
    public static function getByTeacher($teacherId)
    {
        return self::with('client.clientProfile')
            ->where('teacher_id', $teacherId)
            ->where('is_accepted', '')
            ->get();
    }

    /**
     * relation to user table
     * @return BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(User::class, 'client_id', 'id');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i:s');
    }

    /**
     * @param $isAccepted
     * @return mixed
     * @throws \Exception
     */
    public function decide($isAccepted){
        $result = $this->update($isAccepted);

        if ($result){
            return $isAccepted['is_accepted'];
        }else{
            throw new \Exception('TeacherRequests is not updated');
        }
    }

}
