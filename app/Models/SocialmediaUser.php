<?php

namespace App\Models;

class SocialmediaUser extends AbstractModels
{

    public function socialMedia(){
        return $this->hasOne(Socialmedia::class, 'id', 'socialmedia_id');
    }
}
