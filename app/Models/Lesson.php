<?php

namespace App\Models;


class Lesson extends AbstractModels {

    /**
     * get next display order id by for particular course
     *
     * @param $chapterId
     * @return int
     */
    public static function getNextDisplayOrder($chapterId)
    {
        $lessonsCount = self::where('chapter_id', $chapterId)->count();

        return $lessonsCount + 1;
    }

    /**
     * get lessons between two display_orders and by chapter_id
     *
     * @param $first
     * @param $second
     * @param $chapterId
     * @return mixed
     */
    public static function getLessonsBetweenOrders($first, $second, $chapterId)
    {
        return self::where('display_order', '>=', $first)
            ->where('display_order', '<=', $second)
            ->where('chapter_id', $chapterId)
            ->orderBy('display_order')
            ->get();
    }
}
