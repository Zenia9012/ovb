<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Relations\MorphTo;

class ClientLesson extends AbstractModels {

    protected $table = 'client_lesson';

    /**
     * relation
     */
    public function client_group()
    {
        return $this->morphTo();
    }

    /**
     * @return MorphTo
     */
    public function lesson_theme()
    {
        return $this->morphTo();
    }

    /**
     * get data by lessons or themes and by clientTeacherId or groupId
     *
     * @param $id
     * @param $typeClientGroup
     * @param $typeLessonTheme
     * @return mixed
     */
    public static function getByIdClientOrGroup($id, $typeClientGroup, $typeLessonTheme)
    {
        return self::with('lesson_theme', 'client_group')
            ->where('client_group_id', $id)
            ->where('client_group_type', $typeClientGroup)
            ->where('lesson_theme_type', $typeLessonTheme)
            ->get();
    }
}
