<?php

namespace App\Models;


use App\Traits\TeacherModelTrait;

class IndividualLesson extends AbstractModels
{
    use TeacherModelTrait;

    /**
     * relation to ClientTeacher
     * something wrong there
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clientTeachers()
    {
        return $this->morphToMany(
            ClientLesson::class,
            'client_group',
            'client_lesson',
            'lesson_theme_id',
            'client_group_id'
        );
    }

    /**
     * polymorphic relation to client_lesson table
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function clientLessons()
    {
        return $this->morphMany(ClientLesson::class,
            'client_group', 'lesson_theme_type','lesson_theme_id');
    }
}
