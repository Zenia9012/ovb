<?php

namespace App\Models;


class Chapter extends AbstractModels {

    protected $with = ['lessons'];

    /**
     * get next display order id by for particular course
     *
     * @param $courseId
     * @return int
     */
    public static function getNextDisplayOrder($courseId)
    {
        $chaptersCount = self::where('course_id', $courseId)->count();

        return $chaptersCount + 1;
    }

    /**
     * relation to lessons
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lessons()
    {
        return $this->hasMany(Lesson::class, 'chapter_id')->orderBy('display_order');
    }

    /**
     * get chapters between two display_orders and by course_id
     *
     * @param $first
     * @param $second
     * @param $chapterId
     * @return mixed
     */
    public static function getChaptersBetweenOrders($first, $second, $chapterId)
    {
        return self::where('display_order', '>=', $first)
            ->where('display_order', '<=', $second)
            ->where('course_id', $chapterId)
            ->orderBy('display_order')
            ->get();
    }
}
