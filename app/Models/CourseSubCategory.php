<?php

namespace App\Models;


class CourseSubCategory extends AbstractModels
{

    /**
     * get sub category by category id
     * @param $id
     * @return mixed
     */
    public static function getByCategoryId($id)
    {
        return self::where('category_id', $id)->get(['id', 'title']);
    }

    public function category()
    {
        return $this->belongsTo(CourseCategory::class);
    }
}
