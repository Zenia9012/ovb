<?php

namespace App\Models;


use Illuminate\Support\Collection;

class StatusLesson extends AbstractModels {

    /**
     * get a lessons by client_teacher_ids
     *
     * @param $id
     * @return mixed
     */
    public static function getByClientId(Collection $id)
    {
        return self::whereIn('client_teacher_id', $id)->get();
    }
}
