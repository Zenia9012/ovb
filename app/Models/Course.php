<?php

namespace App\Models;


use Illuminate\Support\Facades\Auth;

class Course extends AbstractModels {

    /*
     *  always with chapters
    */
    protected $with = ['chapters'];

    public static function getActiveCourses()
    {
        return self::with(['info', 'subCategory', 'bookmarks'])->where('is_active', 1)->get();
    }

    /**
     * get courses by teacher id
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getByTeacherId($id)
    {
        return self::with(['subCategory', 'info', 'language'])->where('teacher_id', $id)->get();
    }

    /**
     * The roles that belong to the user.
     */
    public function subCategory()
    {
        return $this->belongsToMany(
            CourseSubCategory::class,
            'course_sub_category',
            'course_id',
            'sub_category_id'
        );
    }

    /**
     * relation to courses info
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function info()
    {
        return $this->belongsTo(CourseInfo::class, 'info_id');
    }

    /**
     * relation to language table
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function chapters()
    {
        return $this->hasMany(Chapter::class, 'course_id')->orderBy('display_order');
    }

    /**
     * @return mixed
     */
    public function category()
    {
        return $this->subCategory()->first()->category();
    }

    public function bookmarks()
    {
        return $this->hasMany(CourseBookmark::class, 'course_id')
            ->where('client_id', Auth::id());
    }
}
