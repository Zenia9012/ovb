<?php

namespace App\Models;

use App\User;
use App\Traits\TeacherModelTrait;
use Illuminate\Database\Eloquent\Model;


class TeacherProfile extends AbstractModels {

    use TeacherModelTrait;

    protected $allowedMimeTypes = ['image/jpg', 'image/jpeg', 'image/png'];

    protected $appends = ['full_name'];

    protected $fillable = [
        'teacher_id', 'first_name', 'last_name', 'degree', 'photo', 'phone', 'about_me', 'is_public'
    ];

    /**
     *
     * Get the user's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function teacher()
    {
        return $this->hasOne(User::class);
    }

    public function socialMedia()
    {
        return $this->hasMany(SocialmediaUser::class, 'profile_id', 'id');
    }

    public function teacherCategories(){
        return $this->belongsToMany(
            TeacherCategory::class,
            'teacher_category',
            'teacher_profile_id',
            'teacher_category_id'
        );
    }

}
