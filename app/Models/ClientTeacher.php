<?php

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ClientTeacher extends AbstractModels {

    /**
     * get data by client id
     *
     * @param $id
     */
    public static function getByClientId($id)
    {
        return self::with(['userTeacher.teacherProfile', 'groups'])->where('client_id', $id)->get();
    }

    /**
     * get data by teacher id and status 'active'
     *
     * @param int $id
     * @return mixed
     */
    public static function getByTeacherActiveVerified(int $id)
    {
        return self::with(['user' => function ($item) {
            $item->with('clientProfile');
        }])->where('teacher_id', $id)
            ->where('is_active', 1)
            ->where('is_verified', 1)
            ->get();
    }

    /**
     * relation to user table for client
     *
     * @return BelongsTo
     */
    public function userClient()
    {
        return $this->belongsTo(User::class, 'client_id', 'id');
    }

    /**
     * relation to user table for teacher
     *
     * @return BelongsTo
     */
    public function userTeacher()
    {
        return $this->belongsTo(User::class, 'teacher_id', 'id');
    }

    /**
     * get data by teacher id and status 'active'
     *
     * @param int $id
     * @param     $lessonId
     * @return mixed
     */
    public static function getByTeacherActiveVerifiedWithLesson(int $id, $lessonId)
    {
        return self::with([
            'user' => function ($item) {
                $item->with('clientProfile');
            }])
            ->with(['clientLessons' => function ($lesson) use ($lessonId) {
                $lesson->where('lesson_theme_id', $lessonId)
                    ->where('lesson_theme_type', IndividualLesson::class);
            }])
            ->where('teacher_id', $id)
            ->where('is_active', 1)
            ->where('is_verified', 1)
            ->get();
    }

    /**
     * @return MorphMany
     */
    public function clientLessons()
    {
        return $this->morphMany(ClientLesson::class, 'client_group');
    }

    /**
     * relation to TeacherGroups
     * @return BelongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(
            TeacherGroup::class,
            'client_teacher_group',
            'client_teacher_id',
            'group_id');
    }
}
