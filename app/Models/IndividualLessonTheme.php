<?php

namespace App\Models;


use App\Traits\TeacherModelTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;

class IndividualLessonTheme extends AbstractModels {

    use TeacherModelTrait;

    /**
     * relation to lessons
     *
     * @return HasMany
     */
    public function lessons()
    {
        return $this->hasMany(IndividualLesson::class, 'theme_id');
    }
}
