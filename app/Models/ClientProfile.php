<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ClientProfile extends AbstractModels
{
    protected $allowedMimeTypes = ['image/jpg', 'image/jpeg','image/png'];

    protected $appends = ['full_name', 'photo'];


    /**
     * @return HasOne
     */
    public function client(){
        return $this->hasOne(User::class, 'id','client_id');
    }

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * @param $value
     * @return string
     */
    public function getPhotoAttribute($value)
    {
        return $value ? $value : 'default_user_circle.svg';
    }
}
