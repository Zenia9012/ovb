<?php

namespace App;

use App\Models\Course;
use App\Models\ClientProfile;
use App\Models\CourseCategory;
use App\Models\TeacherProfile;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_teacher',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * relation to course
     *
     * @return BelongsToMany
     */
    public function courses()
    {
        return $this->belongsToMany(Course::class, 'course_user', 'client_id');
    }

    /**
     * @return BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(CourseCategory::class, 'ability_clients', 'client_id', 'course_categories_id');
    }

    /**
     * get all clients and it includes admins
     *
     * @return mixed
     */
    public static function getClients()
    {
        return self::where('is_teacher', 0)->get();
    }

    /**
     * get all teachers
     *
     * @return mixed
     */
    public static function getTeachers()
    {
        return self::where('is_teacher', 1)->get();
    }

    /**
     * relation to client profile table
     *
     * @return HasOne
     */
    public function clientProfile()
    {
        return $this->hasOne(ClientProfile::class, 'client_id', 'id');
    }

    /**
     * relation to teacher profile table
     *
     * @return HasOne
     */
    public function teacherProfile()
    {
        return $this->hasOne(TeacherProfile::class, 'teacher_id', 'id');
    }

    /**
     * get user by email
     * @param $email
     * @return mixed
     */
    public static function getUserByEmail($email)
    {
        return self::where('email', $email)->first();
    }

    /**
     * get all admins
     *
     * @return mixed
     */
    public static function getAdmins()
    {
        return self::where('is_admin', 1)->get();
    }
}
