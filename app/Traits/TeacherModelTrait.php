<?php


namespace App\Traits;


trait TeacherModelTrait {

    /**
     * get data by teacher id, works only if you have teacher_id column
     *
     * @param int $id
     * @return mixed
     */
    public static function getByTeacher(int $id)
    {
        return self::where('teacher_id', $id)->get();
    }

    /**
     * get all public teachers
     * @return mixed
     */
    public static function getPublicTeachers()
    {
        return self::where('is_public', 1)->get();
    }
}
