<?php


namespace App\Traits;

trait Image {

    public $defaultCourseImage = 'img/course_default_image.jpg';
    public $defaultImage = 'img/default.jpg';

    public function imageUpload($request, $field, $path = 'images', $default = null)
    {
        if (!$request->hasFile($field)) {
            if ($default) {
                return $default;
            }

            return $this->defaultImage;
        }

        if (!$request->file($field)->isValid()) {
            if ($default) {
                return $default;
            }

            return $this->defaultImage;
        }

        $name = strtolower(str_random(15));
        $extension = $request->$field->extension();

        $storePath = 'storage/' . $path . '/' . $name . '.' . $extension;

        $request->$field->storeAs('public/' . $path, $name . '.' . $extension);

        return $storePath;
    }

    public function updatePhoto(){
        // todo update and delete
    }

}
