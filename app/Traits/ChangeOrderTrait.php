<?php


namespace App\Traits;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

trait ChangeOrderTrait {

    /**
     * change order if item move to the top
     * @param Collection $list
     * @param array      $data
     * @return Collection
     */
    public function changeOrderToTop(Collection $list, array $data)
    {
        return $list->transform(function (Model $item) use ($data) {
            if ($item->display_order == $data['old_order']) {
                $item->display_order = $data['new_order'];
            } else {
                $item->display_order += 1;
            }
            $item->save();

            return $item;
        });
    }

    /**
     * change order if item move to the bottom
     * @param Collection $list
     * @param array      $data
     * @return Collection
     */
    public function changeOrderToBottom(Collection $list, array $data)
    {
        return $list->transform(function (Model $item) use ($data) {
            if ($item->display_order == $data['old_order']) {
                $item->display_order = $data['new_order'];
            } else {
                $item->display_order -= 1;
            }
            $item->save();

            return $item;
        });
    }
}
