<?php


namespace App\Traits;


use Illuminate\Support\Facades\URL;
use App\Providers\RouteServiceProvider;

trait LoginRedirectable {

    /**
     * define the index in session array
     * @var string
     */
    protected $session_param = 'previous_url';

    /**
     * The URIs that should be excluded from redirection.
     *
     * @var array
     */
    protected $except = [
        '/login',
        '/',
    ];

    /**
     * get route without the domain
     * @param $url
     * @return string|string[]
     */
    protected function getRoute($url)
    {
        return str_replace(env('APP_URL'), '', $url);
    }

    /**
     * store previous route to session
     * @param $route
     */
    protected function storeRouteToSession($route)
    {
        session()->put($this->session_param, $route);
    }

    /**
     *  set redirect to param
     */
    protected function setRedirectToPrevious()
    {
        if (session()->has($this->session_param)) {
            $this->redirectTo = $this->excludeRoutes(session()->pull($this->session_param));
        }else{
            $this->redirectTo = RouteServiceProvider::HOME;
        }
    }

    /**
     * check exclude routes
     * @param $route
     * @return string
     */
    protected function excludeRoutes($route){
        foreach ($this->except as $item){
            if ($route == $item){
                return RouteServiceProvider::HOME;
            }
        }
        return $route;
    }

    protected function setRedirectAfterLogin()
    {
        if (preg_match('/teachers/', URL::previous())){
            $this->redirectTo = route('teacher.cabinet.index');
        }else{
            $this->setRedirectToPrevious();
        }
    }

}
