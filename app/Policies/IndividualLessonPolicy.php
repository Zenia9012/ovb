<?php

namespace App\Policies;

use App\User;
use App\Models\IndividualLesson;
use Illuminate\Auth\Access\HandlesAuthorization;

class IndividualLessonPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any individual lessons.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the individual lesson.
     *
     * @param \App\User        $user
     * @param IndividualLesson $individualLesson
     * @return mixed
     */
    public function view(User $user, IndividualLesson $individualLesson)
    {
        return $user->id == $individualLesson->teacher_id;
    }

    /**
     * Determine whether the user can create individual lessons.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the individual lesson.
     *
     * @param  \App\User  $user
     * @param    $individualLesson
     * @return mixed
     */
    public function update(User $user, IndividualLesson $individualLesson)
    {
        return $user->id == $individualLesson->teacher_id;
    }

    /**
     * Determine whether the user can delete the individual lesson.
     *
     * @param  \App\User  $user
     * @param  $individualLesson
     * @return mixed
     */
    public function delete(User $user, IndividualLesson $individualLesson)
    {
        //
    }

    /**
     * Determine whether the user can restore the individual lesson.
     *
     * @param  \App\User  $user
     * @param  $individualLesson
     * @return mixed
     */
    public function restore(User $user, IndividualLesson $individualLesson)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the individual lesson.
     *
     * @param  \App\User  $user
     * @param  $individualLesson
     * @return mixed
     */
    public function forceDelete(User $user, IndividualLesson $individualLesson)
    {
        //
    }
}
