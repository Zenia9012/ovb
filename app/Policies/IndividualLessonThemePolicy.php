<?php

namespace App\Policies;

use App\User;
use App\Models\IndividualLessonTheme as Theme;
use Illuminate\Auth\Access\HandlesAuthorization;

class IndividualLessonThemePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the individual lesson theme.
     *
     * @param User  $user
     * @param Theme $theme
     * @return mixed
     */
    public function view(User $user, Theme $theme)
    {
        return $user->id == $theme->teacher_id;
    }

    /**
     * Determine whether the user can update the individual lesson theme.
     *
     * @param User  $user
     * @param Theme $theme
     * @return mixed
     */
    public function update(User $user, Theme $theme)
    {
        return $user->id == $theme->teacher_id;
    }

    /**
     * Determine whether the user can delete the individual lesson theme.
     *
     * @param User  $user
     * @param Theme $theme
     * @return mixed
     */
    public function delete(User $user, Theme $theme)
    {
        return $user->id == $theme->teacher_id;
    }
}
