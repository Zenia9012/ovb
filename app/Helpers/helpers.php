<?php

const NOTIFY_SUCCESS = 'success';
const NOTIFY_INFO = 'info';
const NOTIFY_WARNING = 'warning';
const NOTIFY_ERROR = 'error';
const NOTIFY_SESSION_KEY = 'notify_message';

if (!function_exists('notify')) {
    function notify(string $message, $type = NOTIFY_SUCCESS)
    {
        session()->push(NOTIFY_SESSION_KEY . '.' . $type, $message);
    }
}
