<?php

namespace App\Http\Middleware;

use Closure;

class CheckIfTeacherProfileExist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        dd($request->user()->teacherProfile()->get()->count());
        if($request->user()->teacherProfile()->get()->count()){
            return $next($request);
        }
        else{
            return redirect(route('teacher.profile.create'));
        }
    }
}
