<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIfClient {

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() == null) {
            return redirect('/login');
        }

        if ($request->user()->is_teacher == 0) {
            return $next($request);
        }

        Auth::logout();

        return redirect()->route('teacher.login');
    }
}
