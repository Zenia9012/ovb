<?php

namespace App\Http\Controllers\Auth;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Models\SocialAccount;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Contracts\View\Factory;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Foundation\Application;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SocialAuthController extends Controller {

    /**
     * Create a redirect method to google api.
     *
     * @return RedirectResponse
     */
    public function redirect()
    {
        $provider = '';

        if (Route::currentRouteName() == 'login.redirect.facebook') {
            $provider = 'facebook';
        } else if (Route::currentRouteName() == 'login.redirect.google') {
            $provider = 'google';
        } else {
            abort(404, 'Provider is unknown');
        }

        if (preg_match('/teachers/', URL::previous())) {
            session()->push('is_teacher', 1);
        }

        return Socialite::driver($provider)->redirect();
    }

    /**
     * Return a callback method from google api.
     *
     * @return Application|Factory|\Illuminate\Http\RedirectResponse|View
     */
    public function callback()
    {
        $isTeacher = false;
        $redirectTo = '/cabinet';
        $provider = '';

        if (Route::currentRouteName() == 'login.callback.facebook') {
            $provider = 'facebook';
        } else if (Route::currentRouteName() == 'login.callback.google') {
            $provider = 'google';
        } else {
            abort(404, 'Provider is unknown');
        }

        if (session()->has('is_teacher')) {
            $isTeacher = true;
            $redirectTo = 'teachers/cabinet';
            session()->forget('is_teacher');
        }

        $userProvider = Socialite::driver($provider)->user();

        if (!$userProvider->getEmail()) {
            session()->put('user_provider', $userProvider);
            session()->put('provider', $provider);
            session()->put('is_teacher', $isTeacher);
            session()->put('redirect_to', $redirectTo);

            return view('auth.email');
        }

        $user = SocialAccount::createOrGetUser($userProvider, $provider, $isTeacher);

        if ($user) {
            auth()->login($user);

            return redirect()->to($redirectTo);
        }

        return redirect()->to('/');
    }

    /**
     * add email if it's not exists in social auth
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws ValidationException
     */
    public function addEmail(Request $request)
    {
        $validated = $this->validate($request, [
            'email' => 'required|email',
        ]);

        if (
            session()->has('user_provider')
            && session()->has('provider')
            && session()->has('is_teacher')
            && session()->has('redirect_to')
        ) {
            $user = SocialAccount::createOrGetUser(
                session()->pull('user_provider'),
                session()->pull('provider'),
                session()->pull('is_teacher'),
                $validated['email']
            );

            if ($user) {
                auth()->login($user);

                return redirect()->to(session()->pull('redirect_to'));
            }
        }

        return redirect()->to('/');
    }

    /**
     * add telegram id
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function addTelegramId(Request $request)
    {
        $validated = $this->validate($request, [
            'telegram_id' => 'required',
        ]);

        $user = Auth::user();
        $user->telegram_id = $validated['telegram_id'];
        $result = $user->save();

        return response()->json($result);
    }
}
