<?php

namespace App\Http\Controllers\Client;

use Illuminate\View\View;
use Illuminate\Http\Response;
use App\Models\TeacherProfile;
use App\Models\TeacherRequests;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Foundation\Application;

class TeacherController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $teachers = TeacherProfile::getPublicTeachers();
        $requests = TeacherRequests::getRequestsByClientId(Auth::id());

        return view('client.teachers.index', compact(['teachers','requests']));
    }

    /**
     * Display the specified resource.
     *
     * @param TeacherProfile $teacher
     * @return Application|Factory|View
     */
    public function show(TeacherProfile $teacher)
    {
        return view('client.teachers.show', compact('teacher'));
    }


    /**
     * add request to teacher
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function requestToTeacher(Request $request)
    {
        $validated = $this->validate($request, [
            'teacher_id' => 'required|integer',
            'message' => 'nullable',
        ]);

        $teacherRequest = TeacherRequests::create($validated + ['client_id' => Auth::id()]);

        return response()->json($teacherRequest);
    }
}
