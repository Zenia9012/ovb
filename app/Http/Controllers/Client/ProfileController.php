<?php

namespace App\Http\Controllers\Client;

use Illuminate\View\View;
use Illuminate\Routing\Redirector;
use App\Http\Requests\StoreProfile;
use App\Http\Requests\UpdateProfile;
use App\Models\AbilityClient;
use App\Models\ClientProfile;
use App\Models\CourseCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Foundation\Application;

class ProfileController extends Controller {

    /**
     * Display data from profile
     *
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $profile = ClientProfile::where('client_id', Auth::id())->first();
        if ($profile) {
            return view('client.profile.index', compact('profile'));
        }

        return redirect(route('profile.create'));

    }

    /**
     * Show the form for creating the profile.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $profile = ClientProfile::where('client_id', Auth::id())->exists();
        $categories = CourseCategory::all();
        if (!$profile) {
            return view('client.profile.create', compact('categories'));
        }

        return redirect(route('profile.index'));
    }

    /**
     * Store the profile.
     *
     * @param StoreProfile $request
     * @return Application|Factory|View
     */
    public function store(StoreProfile $request)
    {
        $validated = $request->validated();

        //check if profile already exist
        $profile = ClientProfile::where('client_id', Auth::id())->exists();
        if ($profile) {
            return view('client.profile.index', compact('profile'));
        }

        foreach ($validated['abilities'] as $ability) {
            AbilityClient::create(['client_id' => Auth::id(), 'course_categories_id' => $ability]);
        }
        unset($validated['abilities']);

        $fileNameToStore = time() . '.' . $request->file('photo')->getClientOriginalExtension();
        $path = '/storage/avatars/clients/' . $fileNameToStore;

        Image::make($request->file('photo'))->resize(300, 300)->save(public_path($path));
        $validated['photo'] = $path;

        ClientProfile::create($validated + ['client_id' => Auth::id()]);

        return redirect(route('profile.index'));
    }


    /**
     * Show the form for editing the profile.
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function edit()
    {
        $profile = ClientProfile::where('client_id', Auth::id())->first();
        $categories = CourseCategory::all();
        $activeCategories = $profile->client->categories;

        if (!$profile) {
            return redirect(route('profile.create'));
        }

        return view('client.profile.edit', compact('profile', 'categories', 'activeCategories'));
    }

    /**
     * Update the profile.
     *
     * @param UpdateProfile $request
     * @return Application|Factory|View
     */
    public function update(UpdateProfile $request)
    {
        $validated = $request->validated();

        $clientId = Auth::id();
        AbilityClient::where('client_id', $clientId)->delete();

        $newAbilities = $validated['abilities'];
        foreach ($newAbilities as $ability) {
            AbilityClient::create(['client_id' => $clientId, 'course_categories_id' => $ability]);
        }
        unset($validated['abilities']);

        $profile = ClientProfile::where('client_id', Auth::id())->first();
        $profile->update($validated);

        return view('client.profile.index', compact('profile'));
    }

    /**
     * @param Request $request
     */
    public function updatePhoto(Request $request)
    {
        //update photo and delete old
    }
}
