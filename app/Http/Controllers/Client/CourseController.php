<?php

namespace App\Http\Controllers\Client;

use App\Models\Course;
use Illuminate\Http\Request;
use App\Models\CourseBookmark;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class CourseController extends Controller {

    /**
     * show courses for auth teacher
     */
    public function show()
    {
        $user = Auth::user();

        $courses = $user->courses;

        return view('client.courses.my-courses', compact('courses'));
    }

    /**
     * show all courses
     */
    public function index()
    {

    }

    /**
     * @param Request $request
     * @param Course  $course
     * @return JsonResponse
     * @throws ValidationException
     */
    public function bookmarks(Request $request, Course $course)
    {
        $this->validate($request, [
            'bookmarks' => 'required',
        ]);

        $clientId = Auth::id();

        if ($request->bookmarks) {
            CourseBookmark::create([
                'course_id' => $course->id,
                'client_id' => $clientId,
            ]);
        } else {
            CourseBookmark::where('course_id', $course->id)->where('client_id', $clientId)->delete();
        }

        return response()->json(true);
    }
}
