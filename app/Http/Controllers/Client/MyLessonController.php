<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Models\ClientLesson;
use App\Models\TeacherGroup;
use App\Models\StatusLesson;
use App\Models\ClientTeacher;
use App\Models\IndividualLesson;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\IndividualLessonTheme;

class MyLessonController extends Controller {

    public function index()
    {
        $data = ClientTeacher::getByClientId(Auth::id());
        $completedLessons = StatusLesson::getByClientId($data->pluck('id'))->pluck('id');

        foreach ($data as $clientTeacher) {
            $clientTeacher->lessons = ClientLesson::getByIdClientOrGroup(
                $clientTeacher->id,
                ClientTeacher::class,
                IndividualLesson::class
            );
            $clientTeacher->themes = ClientLesson::getByIdClientOrGroup(
                $clientTeacher->id,
                ClientTeacher::class,
                IndividualLessonTheme::class
            );

            foreach ($clientTeacher->groups as $group) {
                $group->lessons = ClientLesson::getByIdClientOrGroup(
                    $group->id,
                    TeacherGroup::class,
                    IndividualLesson::class
                );
                $group->themes = ClientLesson::getByIdClientOrGroup(
                    $group->id,
                    TeacherGroup::class,
                    IndividualLessonTheme::class
                );
            }
        }

        return view('client.my-lessons.index', compact(['data', 'completedLessons']));
    }
}
