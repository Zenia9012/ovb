<?php

namespace App\Http\Controllers\Client;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;

class CabinetController extends Controller
{

    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('client.cabinet');
    }
}
