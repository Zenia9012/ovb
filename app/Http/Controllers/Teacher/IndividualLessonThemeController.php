<?php

namespace App\Http\Controllers\Teacher;

use Exception;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\IndividualLessonTheme as Theme;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Requests\Teacher\StoreUpdateThemeRequest;

class IndividualLessonThemeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $themes = Theme::getByTeacher(Auth::id());

        return response()->json($themes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUpdateThemeRequest $request
     * @return JsonResponse
     */
    public function store(StoreUpdateThemeRequest $request)
    {
        $validated = $request->validated();

        $theme = Theme::create($validated + ['teacher_id' => Auth::id()]);

        return response()->json($theme);
    }

    /**
     * Display the specified resource.
     *
     * @param Theme $theme
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function show(Theme $theme)
    {
        $this->authorize('view', $theme);

        return response()->json($theme);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreUpdateThemeRequest $request
     * @param Theme                   $theme
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function update(StoreUpdateThemeRequest $request, Theme $theme)
    {
        $this->authorize('update', $theme);

        $validated = $request->validated();

        $theme->update($validated);

        return response()->json($theme);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Theme $theme
     * @return JsonResponse
     * @throws Exception
     * @throws AuthorizationException
     */
    public function destroy(Theme $theme)
    {
        $this->authorize('delete', $theme);

        $result = $theme->delete();

        return response()->json($result);
    }
}
