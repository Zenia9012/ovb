<?php

namespace App\Http\Controllers\Teacher;

use Exception;
use Illuminate\View\View;
use App\Models\TeacherGroup;
use App\Models\ClientTeacher;
use App\Models\IndividualLesson;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Requests\Teacher\StoreIndividualLessonRequest;

class IndividualLessonsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $lessons = IndividualLesson::getByTeacher(Auth::id());
        $groups = TeacherGroup::getByTeacher(Auth::id());
        $students = ClientTeacher::getByTeacherActiveVerified(Auth::id());

        return view('teacher.individual_lesson.index', compact(['lessons', 'groups', 'students']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('teacher.individual_lesson.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreIndividualLessonRequest $request
     * @return RedirectResponse
     */
    public function store(StoreIndividualLessonRequest $request)
    {
        $validated = $request->validated();

        IndividualLesson::create(['teacher_id' => Auth::id()] + $validated);

        return redirect()->route('teacher.individual.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param IndividualLesson $lesson
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function edit(IndividualLesson $lesson)
    {
        $this->authorize('update', $lesson);

        $students = ClientTeacher::getByTeacherActiveVerifiedWithLesson(Auth::id(), $lesson->id);

        return view('teacher.individual_lesson.edit', compact(['lesson', 'students']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreIndividualLessonRequest $request
     * @param IndividualLesson             $lesson
     * @return JsonResponse
     */
    public function update(StoreIndividualLessonRequest $request, IndividualLesson $lesson)
    {
        $validated = $request->validated();

        $result = $lesson->update($validated);

        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param IndividualLesson $lesson
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(IndividualLesson $lesson)
    {
        return response()->json($lesson->delete());
    }

    /**
     * add lesson to groups of students
     *
     * @param Request          $request
     * @param IndividualLesson $lesson
     * @return JsonResponse
     * @throws ValidationException
     */
    public function addLessonToGroup(Request $request, IndividualLesson $lesson)
    {
        $validated = $this->validate($request, ['groups' => 'required|array']);

        $lesson->clientTeachers()->attach($validated['groups'], [
            'lesson_theme_type' => IndividualLesson::class,
            'client_group_type' => TeacherGroup::class,
        ]);

        return response()->json(true);
    }

    /**
     * add lesson to student by id
     *
     * @param Request          $request
     * @param IndividualLesson $lesson
     * @return JsonResponse
     * @throws ValidationException
     */
    public function addLessonToStudent(Request $request, IndividualLesson $lesson)
    {
        $validated = $this->validate($request, ['students' => 'required|array']);

        $lesson->clientTeachers()->attach($validated['students'], [
            'lesson_theme_type' => IndividualLesson::class,
            'client_group_type' => ClientTeacher::class,
        ]);

        return response()->json(true);
    }

    /**
     * toggle is_active in lesson
     *
     * @param Request          $request
     * @param IndividualLesson $lesson
     * @return JsonResponse
     * @throws AuthorizationException
     * @throws ValidationException
     */
    public function activate(Request $request, IndividualLesson $lesson)
    {
        $this->authorize('update', $lesson);

        $validated = $this->validate($request, ['is_active' => 'required|boolean']);

        $result = $lesson->update($validated);

        return response()->json($result);
    }

    /**
     * detach student from lesson
     *
     * @param IndividualLesson $lesson
     * @param                  $clientId
     * @return JsonResponse
     */
    public function detachStudent(IndividualLesson $lesson, $clientId)
    {
        $result = $lesson->clientLessons()
            ->where('client_group_id', $clientId)
            ->where('client_group_type', ClientTeacher::class)
            ->delete();

        return response()->json($result);
    }

    /**
     * duplicate lesson
     *
     * @param IndividualLesson $lesson
     * @return JsonResponse
     */
    public function duplicate(IndividualLesson $lesson)
    {
        $duplicatedLesson = $lesson->replicate();
        $duplicatedLesson->title = $duplicatedLesson->title . ' ' . __('individual_lessons.copy_lesson_title_text');
        $duplicatedLesson->is_active = 0;
        $duplicatedLesson->save();

        return response()->json($duplicatedLesson);
    }
}
