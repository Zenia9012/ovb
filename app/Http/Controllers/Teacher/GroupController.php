<?php

namespace App\Http\Controllers\Teacher;

use Exception;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Models\TeacherGroup;
use Illuminate\Http\Response;
use App\Models\ClientTeacher;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\Factory;
use App\Http\Requests\Teacher\StoreUpdateGroupRequest;

class GroupController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $teacherId = Auth::id();

        $groups = TeacherGroup::getTeachersGroupsWithClientInfo($teacherId);
        $clients = ClientTeacher::getByTeacherActiveVerified($teacherId);

        return view('teacher.groups.index', compact(['groups', 'clients']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUpdateGroupRequest $request
     * @return JsonResponse
     */
    public function store(StoreUpdateGroupRequest $request)
    {
        $validated = $request->validated();

        $group = TeacherGroup::create($validated + ['teacher_id' => Auth::id()]);
        $group->clients;

        return response()->json($group);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreUpdateGroupRequest $request
     * @param TeacherGroup            $group
     * @return JsonResponse
     */
    public function update(StoreUpdateGroupRequest $request, TeacherGroup $group)
    {
        $validated = $request->validated();

        return response()->json($group->update($validated));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param TeacherGroup $group
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(TeacherGroup $group)
    {
        return response()->json($group->delete());
    }

    /**
     * @param TeacherGroup $group
     * @param int          $clientId
     * @return JsonResponse
     */
    public function detachClient(TeacherGroup $group, int $clientId)
    {
        return response()->json($group->clients()->detach($clientId));
    }

    /**
     * @param TeacherGroup $group
     * @param int          $clientId
     * @return JsonResponse
     */
    public function attachClient(TeacherGroup $group, int $clientId)
    {
        // methods return void
        $group->clients()->attach($clientId);

        return response()->json(true);
    }
}
