<?php

namespace App\Http\Controllers\Teacher;

use App\Models\Course;
use App\Models\Chapter;
use App\Traits\ChangeOrderTrait;
use App\Http\Controllers\Controller;
use App\Models\Lesson;
use Illuminate\Http\Request;
use App\Http\Requests\Teacher\LessonStoreRequest;
use App\Http\Requests\Teacher\ChangeOrderRequest;

class LessonController extends Controller {

    use ChangeOrderTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Course  $course
     * @param Chapter $chapter
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Course $course, Chapter $chapter)
    {
        return view('teacher.lessons.create', compact(['chapter', 'course']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Course             $course
     * @param Chapter            $chapter
     * @param LessonStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Course $course, Chapter $chapter, LessonStoreRequest $request)
    {
        $validated = $request->validated();

        Lesson::create([
                'chapter_id' => $chapter->id,
                'display_order' => Lesson::getNextDisplayOrder($chapter->id),
            ] + $validated);

        return redirect()->route('teacher.courses.show', $course->id);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Lesson $lesson
     * @return \Illuminate\Http\Response
     */
    public function show(Lesson $lesson)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Course             $course
     * @param Chapter            $chapter
     * @param \App\Models\Lesson $lesson
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Course $course, Chapter $chapter, Lesson $lesson)
    {
        return view('teacher.lessons.edit', compact(['course', 'chapter', 'lesson']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param LessonStoreRequest $request
     * @param Course             $course
     * @param Chapter            $chapter
     * @param \App\Models\Lesson $lesson
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(LessonStoreRequest $request, Course $course, Chapter $chapter, Lesson $lesson)
    {
        $validated = $request->validated();

        $lesson->update($validated);

        return redirect()->route('teacher.courses.show', $course->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Lesson $lesson
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Lesson $lesson)
    {
        $result = $lesson->delete();

        return response()->json($result);
    }

    /**
     * change order for the lesson
     *
     * @param ChangeOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeOrder(ChangeOrderRequest $request)
    {
        $validated = $request->validated();

        if ($validated['old_order'] === $validated['new_order']) {
            return response()->json(false, 200);
        }

        if ($validated['old_order'] > $validated['new_order']) {
            $lessons = Lesson::getLessonsBetweenOrders($validated['new_order'], $validated['old_order'], $validated['key_id']);

            $lessons = $this->changeOrderToTop($lessons, $validated);
        } else {
            $lessons = Lesson::getLessonsBetweenOrders($validated['old_order'], $validated['new_order'], $validated['key_id']);

            $lessons = $this->changeOrderToBottom($lessons, $validated);
        }

        return response()->json($lessons, 201);
    }
}
