<?php

namespace App\Http\Controllers\Teacher;

use App\Traits\ChangeOrderTrait;
use App\Http\Controllers\Controller;
use App\Models\Chapter;
use Illuminate\Http\Request;
use App\Http\Requests\Teacher\ChangeOrderRequest;

class ChapterController extends Controller {

    use ChangeOrderTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'title' => 'required|min:3',
            'course_id' => 'required|integer',
        ]);

        return Chapter::create($validated + ['display_order' => Chapter::getNextDisplayOrder($validated['course_id'])]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Chapter $chapter
     * @return \Illuminate\Http\Response
     */
    public function show(Chapter $chapter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Chapter $chapter
     * @return \Illuminate\Http\Response
     */
    public function edit(Chapter $chapter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Chapter      $chapter
     * @return Chapter
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Chapter $chapter)
    {
        $validated = $this->validate($request, [
            'title' => 'required|min:3',
        ]);
        $chapter->update($validated);

        return $chapter;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Chapter $chapter
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Chapter $chapter)
    {
        $result = $chapter->delete();

        return response()->json($result);
    }

    /**
     * change order for the chapter
     *
     * @param ChangeOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeOrder(ChangeOrderRequest $request)
    {
        $validated = $request->validated();

        if ($validated['old_order'] === $validated['new_order']) {
            return response()->json(false, 200);
        }

        if ($validated['old_order'] > $validated['new_order']) {
            $chapters = Chapter::getChaptersBetweenOrders($validated['new_order'], $validated['old_order'], $validated['key_id']);

            $chapters = $this->changeOrderToTop($chapters, $validated);
        } else {
            $chapters = Chapter::getChaptersBetweenOrders($validated['old_order'], $validated['new_order'], $validated['key_id']);

            $chapters = $this->changeOrderToBottom($chapters, $validated);
        }

        return response()->json($chapters, 201);
    }
}
