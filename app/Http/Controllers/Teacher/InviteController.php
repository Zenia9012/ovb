<?php

namespace App\Http\Controllers\Teacher;

use App\User;
use Exception;
use Illuminate\View\View;
use App\Models\ClientInvite;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Notifications\InviteStudent;
use Illuminate\Contracts\View\Factory;
use App\Notifications\InviteNewStudent;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Foundation\Application;

class InviteController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $invites = ClientInvite::getByTeacher(Auth::id());

        return view('teacher.invite.index', compact('invites'));
    }

    /**
     * added the invite, and send the notification
     *
     * @param Request $request
     * @return bool|JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'email' => 'required|email|unique:client_invites,email',
        ]);

        $client = User::getUserByEmail($validated['email']);

        if ($client) {

            try {
                $invite = ClientInvite::create([
                    'teacher_id' => Auth::id(),
                    'client_id' => $client->id,
                    'is_accepted' => 0
                ]);
            } catch (QueryException $exception) {
                return response()->json(['message' => 'Already added'], 422);
            }

            $client->notify((new InviteStudent($invite)));

            $invite->client->clientProfile;
        } else {
            $invite = ClientInvite::create([
                'teacher_id' => Auth::id(),
                'is_accepted' => 0,
                'token' => str_random(11),
                'email' => $validated['email']
            ]);

            Notification::route('mail', $validated['email'])->notify(new InviteNewStudent($invite));
        }

        return response()->json($invite);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ClientInvite $invite
     * @return bool
     * @throws Exception
     */
    public function destroy(ClientInvite $invite)
    {
        return $invite->delete();
    }
}
