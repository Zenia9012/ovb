<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\View\View;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;

class CabinetController extends Controller {

    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('teacher.cabinet');
    }
}
