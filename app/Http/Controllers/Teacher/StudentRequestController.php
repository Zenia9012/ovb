<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Models\TeacherRequests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\Factory;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Foundation\Application;

class StudentRequestController extends Controller {

    /**
     * show all teacher's requests
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $teachersRequests = TeacherRequests::getByTeacher(Auth::id());

        return view('teacher.students_requests.index', compact('teachersRequests'));
    }

    /**
     * @param TeacherRequests $teacherRequests
     * @param Request         $request
     * @return bool
     * @throws ValidationException
     */
    public function update(TeacherRequests $teacherRequests, Request $request)
    {
        $validated = $this->validate($request, [
            'is_accepted' => 'required|boolean',
        ]);

        $status = $teacherRequests->decide($validated);

        return $status ? __('teachers_students.request_accepted') : __('teachers_students.request_declined');
    }
}
