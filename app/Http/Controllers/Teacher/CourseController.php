<?php

namespace App\Http\Controllers\Teacher;

use App\Models\Course;
use App\Models\Language;
use App\Models\CourseInfo;
use App\Traits\Image;
use App\Models\CourseCategory;
use App\Models\CourseSubCategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Teacher\CourseStoreRequest;

class CourseController extends Controller {

    use Image;

    /**
     * Display a listing of the resource.
     * show all courses
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $courses = Course::getActiveCourses();
        $categories = CourseCategory::all();

        return view('teacher.courses.index', compact(['courses', 'categories']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = CourseCategory::all();
        $languages = Language::all();

        return view('teacher.courses.create', compact(['categories', 'languages']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CourseStoreRequest $request)
    {
        $validated = $request->validated();

        $imagePath = $this->imageUpload($request, 'image', 'img/courses', $this->defaultCourseImage);

        $courseInfo = CourseInfo::create([
            'title' => $validated['title'],
            'description' => $validated['description'],
            'content' => $validated['content'],
        ]);

        $course = Course::create([
            'teacher_id' => Auth::id(),
            'language_id' => 1, // for now it's Ukraine only
            'info_id' => $courseInfo->id,
            'image' => $imagePath,
            'price' => $validated['price'],
            'limit' => $validated['limit'],
        ]);

        $course->subCategory()->attach($validated['sub_category']);

        return redirect()->route('teacher.courses.show', $course->id);
    }

    /**
     * Display the specified resource.
     * show courses for auth teacher
     *
     * @param Course $course
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Course $course)
    {
        $this->authorize('view', $course);

        return view('teacher.courses.show', compact('course'));
    }

    /**
     * Display the specified resource.
     * show courses for auth teacher
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function myCourses()
    {
        $teacherId = Auth::id();

        $courses = Course::getByTeacherId($teacherId);

        return view('teacher.courses.my-courses', compact('courses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Course $course
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Course $course)
    {
        $result = $course->delete();

        return response()->json($result, 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getSubCategory(Request $request)
    {
        $category = $this->validate($request, [
            'category' => 'integer|required',
        ]);

        return CourseSubCategory::getByCategoryId($category['category']);
    }
}
