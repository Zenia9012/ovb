<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Http\Requests\Teacher\StoreProfile;
use App\Http\Requests\Teacher\UpdateProfile;
use App\Models\Socialmedia;
use App\Models\SocialmediaUser;
use App\Models\TeacherCategory;
use App\Models\TeacherProfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Image;

class ProfileController extends Controller
{
    /**
     * Display data from profile
     *
     * @return Response
     */
    public function index()
    {
        $profile = TeacherProfile::where('teacher_id', Auth::id())->first();
        return view('teacher.profile.index', compact('profile'));
    }

    /**
     * Show the form for creating the profile.
     *
     * @return Response
     */
    public function create()
    {
        $profile = TeacherProfile::where('teacher_id', Auth::id())->exists();
        $teacherCategories = TeacherCategory::all();
        if (!$profile) {
            return view('teacher.profile.create', compact('teacherCategories'));
        }

        return redirect(route('teacher.profile.index'));
    }

    /**
     * Store the profile.
     *
     * @param StoreProfile $request
     * @return Response
     */
    public function store(StoreProfile $request)
    {
        $validated = $request->validated();

        //check if profile already exist
        $profile = TeacherProfile::where('teacher_id', Auth::id())->exists();
        if ($profile) {
            return view('teacher.profile.index', compact('profile'));
        }

        if ($request->hasFile('photo')) {
            $fileNameToStore = time() . '.' . $request->file('photo')->getClientOriginalExtension();
            $path = '/storage/avatars/teachers/' . $fileNameToStore;

            Image::make($request->file('photo'))->resize(300, 300)->save(public_path($path));
            $validated['photo'] = $path;
        }

        $profile = TeacherProfile::create($validated + ['teacher_id' => Auth::id()]);

        $profile->teacherCategories()->attach($validated['abilities']);

        $socialMedia = Socialmedia::all();
        foreach ($socialMedia as $media) {
            if (!is_null($request['social_' . $media->title])) {
                SocialmediaUser::create([
                    'profile_id' => $profile->id,
                    'socialmedia_id' => $media->id,
                    'link' => $request['social_' . $media->title]
                ]);
            }
        }

        return redirect(route('teacher.profile.index'));
    }

    /**
     * Show the form for editing the profile.
     *
     * @return Response
     */
    public function edit()
    {
        $profile = TeacherProfile::where('teacher_id', Auth::id())->first();
        return view('teacher.profile.edit', compact('profile'));
    }

    /**
     * Update the profile.
     *
     * @param UpdateProfile $request
     * @return Response
     */
    public function update(UpdateProfile $request)
    {
        $validated = $request->validated();

        $profile = TeacherProfile::where('teacher_id', Auth::id())->first();
        $profile->update($validated);

        return view('teacher.profile.index', compact('profile'));
    }
}
