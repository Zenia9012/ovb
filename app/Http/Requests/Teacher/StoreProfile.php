<?php

namespace App\Http\Requests\Teacher;

use Illuminate\Foundation\Http\FormRequest;

class StoreProfile extends FormRequest
{

    protected function prepareForValidation()
    {
        $this->merge(['is_public' => $this->has('is_public')]);
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => 'nullable|file|max:5098',
            'first_name' => 'required|min:3|max:30',
            'last_name' => 'required|min:3|max:30',
            'degree' => 'required|min:5|max:60',
            'abilities' => 'required|min:1|max:3',  // todo Determine the max number
            'phone' => 'nullable|min:4|max:14',
            'is_public' => 'nullable',
            'about_me' => 'required|min:10|max:1000',
        ];
    }
}
