<?php

namespace App\Http\Requests\Teacher;

use Illuminate\Foundation\Http\FormRequest;

class StoreIndividualLessonRequest extends FormRequest {

    protected function prepareForValidation()
    {
        $this->merge(['is_active' => $this->has('is_active')]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:191',
            'description' => 'required|string|min:3',
            'content' => 'nullable',
            'duration' => 'required|numeric',
            'is_active' => 'required|boolean',
        ];
    }
}
