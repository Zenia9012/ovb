<?php

namespace App\Http\Requests\Teacher;

use Illuminate\Foundation\Http\FormRequest;

class CourseStoreRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sub_category' => 'required|array',
            'image' => 'nullable',
            'title' => 'required|string|max:191',
            'description' => 'required|string|max:9999|min:3',
            'language' => 'nullable',
            'category' => 'required',
            'content' => 'nullable',
            'price' => 'required|numeric',
            'limit' => 'required|numeric',
        ];
    }
}
