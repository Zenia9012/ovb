<?php

namespace App\Observers;

use App\Models\Course;
use App\Models\Lesson;
use App\Models\Chapter;

class CourseObserver
{
    /**
     * Handle the course "created" event.
     *
     * @param  \App\Models\Course  $course
     * @return void
     */
    public function created(Course $course)
    {
        $chapter = Chapter::create([
            'course_id' => $course->id,
            'title' => __('courses.introduction_chapter'),
            'display_order' => 1,
        ]);

        Lesson::create([
            'chapter_id' => $chapter->id,
            'title' => __('courses.welcome_lesson_title'),
            'description' => __('courses.welcome_lesson_desc'),
            'display_order' => 1,
        ]);
    }

    /**
     * Handle the course "updated" event.
     *
     * @param  \App\Models\Course  $course
     * @return void
     */
    public function updated(Course $course)
    {
        //
    }

    /**
     * Handle the course "restored" event.
     *
     * @param  \App\Models\Course  $course
     * @return void
     */
    public function restored(Course $course)
    {
        //
    }
}
