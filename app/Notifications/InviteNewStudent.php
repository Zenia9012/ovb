<?php

namespace App\Notifications;

use App\Models\ClientInvite;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class InviteNewStudent extends Notification
{
    use Queueable;

    /**
     * @var ClientInvite
     */
    private $invite;

    /**
     * Create a new notification instance.
     *
     * @param ClientInvite $invite
     */
    public function __construct(ClientInvite $invite)
    {
        $this->invite = $invite;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('SOWL')
                    ->line('We make your life better')
                    ->line('Just register in our appication')
                    ->action('Go to SOWL', url('/register?key=' .$this->invite->token))
                    ->line('Thank you');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
