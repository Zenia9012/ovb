<?php

namespace App\Providers;

use App\Models\Course;
use App\Policies\CoursePolicy;
use App\Models\IndividualLesson;
use App\Models\IndividualLessonTheme;
use App\Policies\IndividualLessonPolicy;
use App\Policies\IndividualLessonThemePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Course::class => CoursePolicy::class,
        IndividualLesson::class => IndividualLessonPolicy::class,
        IndividualLessonTheme::class => IndividualLessonThemePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
